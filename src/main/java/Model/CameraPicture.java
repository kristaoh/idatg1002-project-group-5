package Model;

import java.time.LocalDateTime;

/**
 * Responsible for storage of data of a picture taken by a camera
 *
 * @author IDATG1002 Group 5
 */
public class CameraPicture extends Picture {

    private LocalDateTime timeTaken;
    private double focalLength;
    private String apertureValue;
    private String exposureTime;
    private int iso;
    private String make;
    private String model;
    private String exposureMode;
    private String shutterSpeedValue;
    private String software;
    private String artist;
    private String flash;
    private double brightnessValue;
    private String colorSpace;

    public CameraPicture(int pictureID, String name, String path, String resolution, long fileSize, LocalDateTime timeTaken, double focalLength, String apertureValue, String exposureTime, int iso, String make, String model, String exposureMode, String shutterSpeedValue, String software, String artist, String flash, double brightnessValue, String colorSpace) {
        super(pictureID, name, path, resolution, fileSize);
        this.timeTaken = timeTaken;
        this.focalLength = focalLength;
        this.apertureValue = apertureValue;
        this.exposureTime = exposureTime;
        this.iso = iso;
        this.make = make;
        this.model = model;
        this.exposureMode = exposureMode;
        this.shutterSpeedValue = shutterSpeedValue;
        this.software = software;
        this.artist = artist;
        this.flash = flash;
        this.brightnessValue = brightnessValue;
        this.colorSpace = colorSpace;
    }

    /**
     * Gets the focal length
     * @return focal length
     */
    public double getFocalLength() {
        return focalLength;
    }

    /**
     * Gets the aperture value
     * @return aperture value
     */
    public String getApertureValue() {
        return apertureValue;
    }

    /**
     * Gets the exposure time
     * @return exposure time
     */
    public String getExposureTime() {
        return exposureTime;
    }

    /**
     * Gets the ISO value
     * @return ISO value
     */
    public int getIso() {
        return iso;
    }

    /**
     * Gets the time taken
     * @return time taken
     */
    public LocalDateTime getTimeTaken() {
        return timeTaken;
    }

    /**
     * Gets the time taken in string format
     * @return string of time taken
     */
    public String getTimeTakenToString() {
        if (timeTaken != null) {
            return (timeTaken.getDayOfMonth() < 10 ? "0" + timeTaken.getDayOfMonth() : timeTaken.getDayOfMonth()) + "/" + (timeTaken.getMonthValue() < 10 ? "0" + timeTaken.getMonthValue() : timeTaken.getMonthValue()) + "/" + timeTaken.getYear() + " at " + (timeTaken.getHour() < 10 ? "0" + timeTaken.getHour() : timeTaken.getHour()) + ":" + (timeTaken.getMinute() < 10 ? "0" + timeTaken.getMinute() : timeTaken.getMinute());
        } else {
            return null;
        }

    }

    /**
     * Gets the make
     * @return make
     */
    public String getMake() {
        return make;
    }

    /**
     * Gets the model
     * @return model
     */
    public String getModel() {
        return model;
    }

    /**
     * Gets the exposure mode
     * @return exposure mode
     */
    public String getExposureMode() {
        return exposureMode;
    }

    /**
     * Gets the shutter speed
     * @return shutter speed
     */
    public String getShutterSpeedValue() {
        return shutterSpeedValue;
    }

    /**
     * Gets the software
     * @return software used
     */
    public String getSoftware() {
        return software;
    }

    /**
     * Gets the artist
     * @return artist
     */
    public String getArtist() {
        return artist;
    }

    /**
     * Gets the flash
     * @return flash
     */
    public String getFlash() {
        return flash;
    }

    /**
     * Gets the brightness value
     * @return brightness value
     */
    public double getBrightnessValue() {
        return brightnessValue;
    }

    /**
     * Gets the color space
     * @return color space
     */
    public String getColorSpace() {
        return colorSpace;
    }
}
