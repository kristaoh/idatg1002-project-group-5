package Model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;


/**
 * Responsible for storing data about pictures
 *
 * @author IDATG1002 Group 5
 */
public class Picture implements Serializable {

    private ArrayList<String> tagList;

    private int pictureID;
    private boolean pdfExport;
    private String name;
    private String path;
    private String resolution;
    private long fileSize;
    private LocalDateTime timeLoaded;

    public Picture(int pictureID, String name, String path, String resolution, long fileSize) {
        this.pictureID = pictureID;
        pdfExport = false;
        this.name = name;
        this.path = path;
        this.resolution = resolution;
        this.fileSize = fileSize;
        this.timeLoaded = LocalDateTime.now();
        tagList = new ArrayList<>();
    }



    /**
     * Adds tag to the tagList
     * @param tag tag to be added
     */
    public void addToTagList(String tag) {
        tagList.add(tag);
    }

    /**
     * Removes tag from the tagList
     * @param tag tag to be removed
     */
    public void removeFromTagList(String tag) {
        tagList.remove(tag);
    }

    /**
     * Gets pictureID
     * @return pictureID
     */
    public int getPictureID() {
        return pictureID;
    }

    /**
     * get variabel which tells if file is to be exported to pdf or not
     * @return true or false
     */
    public boolean getPDFExport() { return pdfExport; }

    /**
     * get name
     * @return name of picture
     */
    public String getName() { return name; }

    /**
     * get path
     * @return path of picture
     */
    public String getPath() {
        return path;
    }

    /**
     * get resolution
     * @return resolution of picture
     */
    public String getResolution() {
        return resolution;
    }

    /**
     * get file size
     * @return size of the picture
     */
    public long getFileSize() {
        return fileSize;
    }

    /**
     * get time the image last was loaded in
     * @return date
     */
    public LocalDateTime getTimeLoaded() {
        return timeLoaded;
    }

    /**
     * gets time loaded in string form
     * @return string of date
     */
    public String getTimeLoadedToString(){
        return (timeLoaded.getDayOfMonth() < 10 ? "0" + timeLoaded.getDayOfMonth() : timeLoaded.getDayOfMonth() ) + "/" + (timeLoaded.getMonthValue() < 10 ? "0" + timeLoaded.getMonthValue() : timeLoaded.getMonthValue() ) + "/" + timeLoaded.getYear() + " at " + (timeLoaded.getHour() < 10 ? "0" + timeLoaded.getHour() : timeLoaded.getHour()) + ":" + (timeLoaded.getMinute() < 10 ? "0" + timeLoaded.getMinute() : timeLoaded.getMinute());
    }

    /**
     * gets the tag list
     * @return tag list of picture
     */
    public ArrayList<String> getTagList() {
        return tagList;
    }

    /**
     * adds tag
     * @param tag the tag to be added
     */
    public void addTag(String tag) {
        tagList.add(tag);
    }

    /**
     * sets the pdfExport variabel
     * @param pdfExport true or false
     */
    public void setPDFExport(boolean pdfExport)
    {
        this.pdfExport = pdfExport;
    }

    /**
     * Sets the ID of the picture to the desired given ID
     * @param pictureID new ID of the picture
     */
    public void setPictureID(int pictureID)
    {
        this.pictureID = pictureID;
    }
}