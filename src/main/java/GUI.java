import Model.FileManager;
import Model.PictureFactory;
import Model.Picture;
import Model.CameraPicture;

import com.itextpdf.text.DocumentException;
import com.mysql.jdbc.PacketTooBigException;
import javafx.application.Application;
import javafx.geometry.*;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.controlsfx.control.PopOver;
import org.controlsfx.control.RangeSlider;
import org.controlsfx.control.spreadsheet.Grid;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Responsible for the Graphical User Inteface
 *
 * @author IDATG1002 Group 5
 */
public class GUI extends Application {
    private FileManager fileManager;
    private FileChooser fileChooser;
    private PictureFactory pictureFactory;

    /* ------------
        Variables
       ------------ */
    private static final int defaultWidth = 1280;
    private static final int defaultHeight = 720;

    private static final int leftPaneWidth = 300;
    private static final int rightPaneWidth = 700;

    DropShadow dropShadow;

    public static void main(String[] args) {
        launch(args);
    }

    /*|---------------------------------|
                 START & STOP
      |---------------------------------|*/

    // ------- All available scenes -------
    private static Stage primaryStage;
    private static Scene mainScene;
    private Scene searchScene;
    private Scene previewScene;

    /**
     * Starts the application
     * @param primaryStage the primary stage
     */
    @Override
    public void start(Stage primaryStage) {
        fileManager = new FileManager();

        pictureFactory = PictureFactory.getInstance();

        // Setting up Scenes
        GUI.primaryStage = primaryStage;

        searchScene = setSearchScene();
        previewScene = setPreviewScene();
        mainScene = setMainScene();


        //Global effects
        dropShadow = new DropShadow();
        dropShadow.setRadius(5.0);
        dropShadow.setOffsetX(3.0);
        dropShadow.setOffsetY(3.0);
        dropShadow.setColor(Color.LIGHTGREY);

        Scene loginScene = setLoginScene();

        // Stage stuff
        primaryStage.setTitle("Image Database Program");
        primaryStage.getIcons().add(new Image("icons/app.png"));
        primaryStage.setScene(loginScene);
        primaryStage.setMinWidth(defaultWidth);
        primaryStage.setMinHeight(defaultHeight);
        primaryStage.show();
    }

    /**
     * Stops the application
     */
    @Override
    public void stop() {
        System.exit(0);
    }

    /*|---------------------------------|
                  #LOGIN SCENE
      |---------------------------------|*/

    /**
     * Sets the login scene
     * @return the login scene
     */
    private Scene setLoginScene(){
        BorderPane root = new BorderPane();
        root.getStyleClass().add("background");

        // Login setup
        Label usernameLabel = new Label("Username:");
        Label passwordLabel = new Label("Password:");
        TextField username = new TextField();
        username.setPromptText("Username");
        PasswordField password = new PasswordField();

        Button loginButton = new Button("Login");
        loginButton.setOnAction(e-> {
            boolean success = false;

            try {
                fileManager.setDBLoginDetails(username.getText(), password.getText());
                fileManager.testDBConnection();
                fileManager.createDBStructureIfNotExistent();
                updateHelpText();
                success = true;
            } catch (SQLException ex)
            {
                createAlert("Login Failure", "Username or Password is not correct", Alert.AlertType.INFORMATION);
            }

            if(success){
                createAlert("Login Success", "", Alert.AlertType.INFORMATION);
                //login logic
                boolean directoryExists = fileManager.checkForExistingDirectory();
                if(directoryExists){
                    primaryStage.setScene(setDirectroySetupScene(true));
                } else {
                    primaryStage.setScene(setDirectroySetupScene(false));
                }
            }
        });

        Button cancelButton = new Button("Cancel");
        cancelButton.setOnAction(e-> stop());

        ImageView loginImage = new ImageView(new Image(getClass().getResourceAsStream("icons/app.png"), 128, 128, false, false));

        GridPane loginForm = new GridPane();
        loginForm.setMaxSize(200, 100);
        loginForm.getStyleClass().add("element");
        loginForm.setEffect(dropShadow);
        GridPane.setHalignment(cancelButton, HPos.RIGHT);
        GridPane.setHalignment(loginButton, HPos.LEFT);
        GridPane.setHalignment(loginImage, HPos.CENTER);
        loginForm.setScaleX(2);
        loginForm.setScaleY(2);
        loginForm.setAlignment(Pos.CENTER);
        loginForm.setHgap(10);
        loginForm.setVgap(10);
        loginForm.add(loginImage, 1,0);
        loginForm.add(usernameLabel, 1,1);
        loginForm.add(username, 1,2);
        loginForm.add(passwordLabel,1,3);
        loginForm.add(password,1,4);
        loginForm.add(loginButton, 1,5);
        loginForm.add(cancelButton, 1,5);

        root.setCenter(loginForm);
        Scene scene = new Scene(root, defaultWidth, defaultHeight);

        KeyCode[] konamiCode = {KeyCode.UP, KeyCode.UP, KeyCode.DOWN, KeyCode.DOWN, KeyCode.LEFT, KeyCode.RIGHT, KeyCode.LEFT, KeyCode.RIGHT, KeyCode.A, KeyCode.B, KeyCode.ENTER};
        ArrayList<KeyCode> keyList = new ArrayList<>();
        scene.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
            keyList.add(key.getCode());
            if(Arrays.deepEquals(keyList.toArray(),konamiCode)){
                try {
                    fileManager.setDBLoginDetails("kristaoh", "85SxS9r9");
                    fileManager.testDBConnection();
                    fileManager.createDBStructureIfNotExistent();
                    updateHelpText();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                boolean directoryExists = fileManager.checkForExistingDirectory();
                if(directoryExists){

                    primaryStage.setScene(setDirectroySetupScene(true));
                } else {
                    primaryStage.setScene(setDirectroySetupScene(false));
                }
            }
            if(keyList.size() >= konamiCode.length){
                keyList.clear();
            }
        });

        Button secret = new Button("Secret");
        secret.setStyle("-fx-background-color: rgba(0,0,0,0);" + "-fx-text-fill: rgba(0,0,0,0)");
        secret.setOnAction(e->{
            keyList.clear();
            username.setDisable(true);
            password.setDisable(true);
        });
        root.setBottom(secret);

        scene.getStylesheets().add(getClass().getResource("style/main.css").toExternalForm());
        return scene;
    }

    /**
     * Method for creating a scene where the user can choose which directroy they want to use:
     * IF directory already exists -> prompt user to use either new or existing
     * IF NOT -> prompt user only to use new
     * @param directoryExists true or false based on directory existence
     * @return returns the directory scene
     */
    private Scene setDirectroySetupScene(boolean directoryExists){
        Button newDirectoryButton = new Button("Choose New");
        newDirectoryButton.setOnAction(e-> {
            DirectoryChooser chooser = new DirectoryChooser();
            chooser.setTitle("Choose picture folder");
            String initialDirectoryString;
            if (!System.getProperty("os.name").equals("Mac OS X")) // Checks which OS user is on
            {
                // Makes path according to user system
                initialDirectoryString = System.getProperty("user.home") + "\\Pictures";
            } else {
                // Makes path according to user system
                initialDirectoryString = System.getProperty("user.home") + "/Pictures";
            }
            File defaultDirectory = new File(initialDirectoryString);
            chooser.setInitialDirectory(defaultDirectory);
            File selectedDirectory = chooser.showDialog(primaryStage);

            if (selectedDirectory != null && selectedDirectory.isDirectory())
            {

                try {
                    fileManager.addNewDirectory(selectedDirectory);
                    fileManager.updateDataFromFolder();
                    if (fileManager.getSize() > 0)
                    {
                        updateSearchModifiers();
                    } else {
                        updateFileStatus();
                    }
                    // Refreshes search so pictures from database show up
                    refreshSearch(false);
                    primaryStage.setScene(mainScene);
                } catch (SQLException ex) {
                    createAlert("Error", "Could not create new picture directory", Alert.AlertType.ERROR);
                    ex.printStackTrace();
                }

            }
        });

        Button useExistingDirectoryButton = new Button("Use Existing");
        useExistingDirectoryButton.setOnAction(e-> {

            try {
                fileManager.useExisitingDirectory();
                fileManager.updateDataFromFolder();
                if (fileManager.getSize() > 0)
                {
                    updateSearchModifiers();
                } else {
                    updateFileStatus();
                }
                // Refreshes search so pictures from database show up
                refreshSearch(false);
                primaryStage.setScene(mainScene);
            } catch (SQLException ex) {
                createAlert("Error", "Could not get exisiting picture directory", Alert.AlertType.ERROR);
                ex.printStackTrace();
            }
        });

        Text text = new Text();
        if(directoryExists){
            text.setText("Directory already exists, would you like to ?");
        } else {
            text.setText("No directory exists, would you like to ?");
        }

        useExistingDirectoryButton.setDisable(!directoryExists);

        GridPane directoryForm = new GridPane();
        directoryForm.setMaxSize(100, 200);
        directoryForm.setEffect(dropShadow);
        directoryForm.getStyleClass().add("element");
        GridPane.setHalignment(newDirectoryButton, HPos.LEFT);
        GridPane.setHalignment(useExistingDirectoryButton, HPos.RIGHT);
        directoryForm.getStyleClass().add("element");
        directoryForm.setAlignment(Pos.CENTER);
        directoryForm.setScaleX(2);
        directoryForm.setScaleY(2);
        directoryForm.setHgap(10);
        directoryForm.setVgap(10);
        directoryForm.add(text, 1, 1,2,1);
        directoryForm.add(newDirectoryButton, 1,2,2,1);
        directoryForm.add(useExistingDirectoryButton, 2,2,2,1);

        VBox root = new VBox();
        root.setAlignment(Pos.CENTER);
        root.getStyleClass().add("background");
        root.getChildren().add(directoryForm);
        Scene scene = new Scene(root, defaultWidth, defaultHeight);
        scene.getStylesheets().add(getClass().getResource("style/main.css").toExternalForm());
        return scene;
    }

    /*|---------------------------------------------------------------------------------|
                                         MAIN SCENE
      |---------------------------------------------------------------------------------|*/
    private HBox toolBar = new HBox();
    VBox helpNode = new VBox();

    /**
     * Method used for creating a main scene
     *
     * @return a finished scene
     */
    private Scene setMainScene() {

        /* ------------
             MENU TITLE
           ------------ */
        Label title = new Label("What would you like to do?");
        title.setTextAlignment(TextAlignment.CENTER);
        title.setAlignment(Pos.CENTER);
        title.getStyleClass().add("mainTitle");
        title.setPadding(new Insets(0, 0, 50, 0));
        title.setScaleX(2);
        title.setScaleY(2);

        /* ------------
            SEARCH PANE
           ------------ */
        VBox search = createMainSceneButton("Press to SEARCH images", "icons/search.png", "searchButton", false);

        /* ------------
            UPLOAD PANE
           ------------ */
        VBox upload = createMainSceneButton("Press to UPLOAD image", "icons/upload.png", "uploadButton", true);

        /* ------------
             TOOL BAR
           ------------ */

        Text aboutText = new Text("Image processing software" + "\n"
        + "V 1.0" + "\n"
        + "Software made by Group 5" + "\n"
                + "\n"
        + "All icons is from Icons8:" + "\n"
        + "https://icons8.com/icons/material-rounded");
        aboutText.setTextAlignment(TextAlignment.CENTER);
        VBox aboutNode = new VBox();
        aboutNode.setPadding(new Insets( 5,5,5,5));
        aboutNode.getChildren().add(aboutText);
        Button aboutButton = createButtonWithPopOver("About", "About", "icons/info.png", aboutNode);

        VBox iconGuide = new VBox();
        iconGuide.getChildren().addAll(
                createTextWithPictureIcon("icons/edit.png", new Label(" - the edit button, used for enabling the edit mode")),
                createTextWithPictureIcon("icons/pdf.png", new Label(" - the PDF export, used for exporting all selected images")),
                createTextWithPictureIcon("icons/add_image.png", new Label(" - the add/select button, used for enabling the add mode and selecting images")),
                createTextWithPictureIcon("icons/upload.png", new Label(" - This is the upload button, press this to upload an image")),
                createTextWithPictureIcon("icons/search.png", new Label(" - This is the search button, press this to view all the images")),
                createTextWithPictureIcon("icons/cloud.png", new Label(" - These cloud image(with variants) shows the file status related to DB")),
                createTextWithPictureIcon("icons/info.png", new Label("You can also hover over a button to see it's function!"))
        );


        helpNode.setPadding(new Insets( 5,5,5,5));
        helpNode.getChildren().addAll(new Text(), iconGuide);
        Button helpButton = createButtonWithPopOver("Help", "Help", "icons/help.png", helpNode);

        //Create container for buttons
        toolBar.setAlignment(Pos.CENTER);
        toolBar.setSpacing(20);
        toolBar.getChildren().addAll(helpButton, aboutButton);

        /* ------------
               ROOT
           ------------ */
        // Create root node
        GridPane root = new GridPane();
        root.setAlignment(Pos.CENTER);
        root.getStyleClass().add("mainBackground");
        root.add(title, 2,1,1,1);
        root.add(upload, 1,2,1,1);
        root.add(search, 3,2,1,1);
        root.add(toolBar,2,3,1,1);

        // Create and return scene
        Scene scene = new Scene(root, defaultWidth, defaultHeight);
        scene.getStylesheets().add(getClass().getResource("style/main.css").toExternalForm());
        return scene;
    }

    /**
     * Updates the help text
     */
    public void updateHelpText()
    {
        String supportedFormats = fileManager.allowedFormatsToString().replace('[', ' ').replace(']', ' ');
        double uploadSizeLimit = fileManager.getUploadSizeLimit();
        Text helpText = new Text("How to use this software:" + "\n"
                + "To close this dialog simple click anywhere within the program..." + "\n"
                + "... or drag and drop this window away from the button and use the cross" + "\n"
                + "Uploading: " + "\n"
                + "Supported file formats: " + supportedFormats + " Max file size: " + uploadSizeLimit + " Megabytes\n"
                + "1) Click [Upload], and follow the necessary steps when selecting an image." + "\n"
                + "2) Click [Confirm] if this is the desired image, [Cancel] if not." + "\n"
                + "3) Image is now uploaded, and you can add tags if desired" + "\n"
                + " " + "\n"
                + "Exporting: " + "\n"
                + "1) Click [Add Mode], which enables the selection mode " + "\n"
                + "2) Click [Select] on the individual images to add them" + "\n"
                + "3) Click [Export to PDF], and choose a name and location" + "\n"
                + " " + "\n"
                + "Operating Search: " + "\n"
                + "On the top of the page, you'll find the search bar" + "\n"
                + "On the left, there is a panel for adjusting the search parameters" + "\n"
                + "Below you'll see the tag panel, you can here select images based on existing tags" + "\n"
                + "Above you'll see the tool bar, you can here enable Edit or Add, Export to PDF and the Database Status" + "\n"
                + "If you want to take a closer look at an image, just click on the square preview..." + "\n"
                + "... and you'll be directed to the preview page." + "\n"
                + "Red outline: image doesn't exist in database. Green outline: image exists database" + "\n"
                + " " + "\n"
                + "Adding tags" + "\n"
                + "1) Click [Edit] when previewing an image" + "\n"
                + "2) Write a suitable name for the tag" + "\n"
                + "2) When done, Click [Done]." + "\n"
                + " " + "\n");

        helpNode.getChildren().set(0, helpText);
    }

    /**
     * Method for creating the buttons on the main scene
     * @param buttonText the text displayed under the button
     * @param buttonImagePath path to the image displayed on the button
     * @param cssName name of the css class used to style the button
     * @param isUpload boolean deciding of the button acts as an upload or search
     * @return a VBox acting as a custom button
     */
    private VBox createMainSceneButton(String buttonText, String buttonImagePath, String cssName, boolean isUpload){
        Label text = new Label(buttonText);
        text.getStyleClass().add("mainText");

        Button button = createButtonWithImage("", buttonImagePath, 250, 250);
        button.getStyleClass().add(cssName);
        if(isUpload){
            button.setOnAction(e -> onUploadButtonAction());
        } else {
            button.setOnAction(e -> primaryStage.setScene(searchScene));
        }

        VBox upload = new VBox();
        upload.setAlignment(Pos.CENTER);
        upload.getChildren().addAll(button, text);

        return upload;
    }

    /**
     * Method containing logic behind the upload button
     */
    private void onUploadButtonAction() {
        {
            fileChooser = new FileChooser();

            // get the file selected
            File file = fileChooser.showOpenDialog(primaryStage);

            // If file is selected
            if (file != null) {

                // Updates the current scene
                primaryStage.setScene(previewScene);

                // Checks if the file has the correct format
                if (fileManager.formatCheck(file)) {

                    // Checks if the file is within the size limit
                    if (fileManager.sizeCheck(file))
                    {
                        Picture pictureDisplay;
                        pictureDisplay = pictureFactory.createPicture(-1, file);

                        // Confirm button
                        confirm.setOnAction(e2 -> {
                            //try {
                                //fileManager.copyFile(file);

                                try {
                                    fileManager.uploadFileToDB(file);
                                    fileManager.uploadFileToFolder(file);
                                    Picture finalPictureDisplay = fileManager.getFile(fileManager.getSize()-1);
                                    updatePreview(finalPictureDisplay, searchScene);
                                    primaryStage.setScene(previewScene);
                                    refreshSearch(false);
                                    updateSearchModifiers();

                                    confirmationBox.setVisible(false);
                                    createAlert("Upload Succeeded", "File uploaded to database", Alert.AlertType.CONFIRMATION);


                                } catch (IOException e) {
                                    if (e instanceof FileNotFoundException)
                                    {
                                        primaryStage.setScene(mainScene);
                                        createAlert("Upload failed", "File not found", Alert.AlertType.ERROR);
                                    }
                                    else if (e instanceof FileAlreadyExistsException)
                                    {
                                        primaryStage.setScene(mainScene);
                                        createAlert("Upload failed", "The file you are trying to upload already exists in the database", Alert.AlertType.ERROR);
                                    }
                                    else {
                                        primaryStage.setScene(mainScene);
                                        createAlert("Upload failed", "Failed to upload file to database, IO exception", Alert.AlertType.ERROR);
                                    }
                                    e.printStackTrace();

                                } catch (SQLException e)
                                {
                                    if (e instanceof PacketTooBigException)
                                    {
                                        primaryStage.setScene(mainScene);
                                        createAlert("Upload failed", "Size of the file you are trying to upload is too big \nPacketTooBigException: increase 'max_allowed_packet_size' in phpMyAdmin", Alert.AlertType.ERROR);
                                    }
                                    else {
                                        primaryStage.setScene(mainScene);
                                        createAlert("Upload failed", "Failed to upload file to database, SQL exception", Alert.AlertType.ERROR);
                                        e.printStackTrace();
                                    }
                                }

                            // Enabled tagView
                            tagView.setVisible(true);
                        });

                        // Cancel button
                        cancel.setOnAction(e2 -> {
                            primaryStage.setScene(mainScene);
                            createAlert("Upload failed" , "Upload cancelled by user", Alert.AlertType.INFORMATION);
                            tagView.setVisible(true);
                        });

                        // Updates the page with the confirmation box and the selected image
                        updatePreview(pictureDisplay, mainScene);
                        confirmationBox.setVisible(true);
                        // Disables tagView
                        tagView.setVisible(false);
                    } else {
                        primaryStage.setScene(mainScene);
                        createAlert("Upload failed", "File size is not within the allowed range. (50 Megabytes)", Alert.AlertType.ERROR);
                    }

                } else {
                    primaryStage.setScene(mainScene);
                    createAlert("Upload failed", "File format is not supported. Click help for supported formats", Alert.AlertType.ERROR);
                }


            } else {
                createAlert("Upload Aborted", "No File Selected!", Alert.AlertType.INFORMATION);
            }
        }
    }

    /*|---------------------------------------------------------------------------------|
                                        SEARCH SCENE
      |---------------------------------------------------------------------------------|*/

    // ------- Search scene search modifier elements -------
    private Label fileSizeLabel;
    private TilePane galleryView;
    private TextField searchField;
    private RangeSlider isoSlider;
    private FlowPane tagToggleView;
    private RangeSlider focalSlider;
    private CheckBox portraitButton;
    private CheckBox landscapeButton;
    private RangeSlider fileSizeSlider;
    private RangeSlider resolutionSlider;
    private RangeSlider brightnessSlider;
    StackPane cloudButtons = new StackPane();
    private AtomicBoolean deletionModeEnabled;
    private ArrayList<String> selectedSearchTags;
    private AtomicBoolean addToCollectionModeEnabled;

    /**
     * Method used for creating a search scene
     *
     * @return a finished scene
     */
    private Scene setSearchScene() {

        /*--------------------------------------------------
                              LEFT PANE
          -------------------------------------------------- */

        /* ------------
           BACK BUTTON
           ------------ */
        Button backButton = createButtonWithImage("Back", "icons/back_button.png", 25,25);
        backButton.setOnAction(e-> primaryStage.setScene(mainScene));
        backButton.getStyleClass().add("backButton");

        /* ------------
            TOOL BAR
           ------------ */

        // Atomic booleans for edit and add buttons
        deletionModeEnabled = new AtomicBoolean(false);
        addToCollectionModeEnabled = new AtomicBoolean(false);

        Button editButton = createToolBarButton("icons/edit.png", 25, 25);
        editButton.setTooltip(new Tooltip("Edit Mode"));
        editButton.setOnAction(e -> {
            if (addToCollectionModeEnabled.get()) // Checks if conflicting mode is enabled
            {
                // Switch conflicting mode
                addToCollectionModeEnabled.set(searchAddToCollectionMode(addToCollectionModeEnabled.get()));
            }
            // Enable / disable requested mode
            deletionModeEnabled.set(searchDeletionMode(deletionModeEnabled.get()));
        });

        Button addButton = createToolBarButton("icons/add_image.png", 25, 25);
        addButton.setTooltip(new Tooltip("Add Mode"));
        addButton.setOnAction(e -> {
            if (deletionModeEnabled.get()) // Checks if conflicting mode is enabled
            {
                // Switch conflicting mode
                deletionModeEnabled.set(searchDeletionMode(deletionModeEnabled.get()));
            }
            // Enable / disable requested mode
            addToCollectionModeEnabled.set(searchAddToCollectionMode(addToCollectionModeEnabled.get()));

        });

        Button exportButton = createToolBarButton("icons/pdf.png",25,25);
        exportButton.setTooltip(new Tooltip("Export to PDF"));
        exportButton.setOnAction(e -> {
            fileChooser = new FileChooser();
            // Sets the initial directory
            String initialDirectoryString;
            if (!System.getProperty("os.name").equals("Mac OS X")) // Checks which OS user is on
            {
                // Makes path according to user system
                initialDirectoryString = System.getProperty("user.home") + "\\Downloads";
            } else {
                // Makes path according to user system
                initialDirectoryString = System.getProperty("user.home") + "/Downloads";
            }

            File initialDirectory = new File(initialDirectoryString);
            fileChooser.setInitialDirectory(initialDirectory);
            // Default name of the file
            fileChooser.setInitialFileName("gallery.pdf");
            // Adds pdf extension filter
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PDF files (*.pdf)","*.pdf");
            fileChooser.getExtensionFilters().add(extFilter);
            // Chooses directory of the file
            File dir = fileChooser.showSaveDialog(primaryStage);
            try {
                fileManager.exportToPDF(dir);
                createAlert("Export PDF Succeeded", "PDF has been exported", Alert.AlertType.INFORMATION);
            } catch (NullPointerException ex) {
                createAlert("Export PDF Failed" , "No files selected to be exported", Alert.AlertType.ERROR);
                ex.printStackTrace();
            } catch (IOException ex) {
                if (ex instanceof FileNotFoundException)
                {
                    createAlert("Export PDF Failed" , "One or more of the files selected to be exported could not be located", Alert.AlertType.ERROR);
                }
                else {
                    createAlert("Export PDF Failed" , "IO exception occured", Alert.AlertType.ERROR);
                }
                ex.printStackTrace();
            } catch (DocumentException ex) {
                createAlert("Export PDF Failed" , "Document exception has occured", Alert.AlertType.ERROR);
                ex.printStackTrace();
            }
        });

        Button upToDateDBButton = createToolBarButton("icons/cloud.png",25,25);
        upToDateDBButton.setTooltip(new Tooltip("Everything is saved and up to date"));
        upToDateDBButton.setVisible(false);
        upToDateDBButton.setDisable(true);
        Button pullDBButton = createToolBarButton("icons/cloud_pull.png",25,25);
        pullDBButton.setTooltip(new Tooltip("Some Database images have not been loaded into the folder yet"));
        pullDBButton.setVisible(false);
        pullDBButton.setOnAction(e-> {
            try {
                fileManager.DownloadDataFromDB();
                updateSearchModifiers();
                // Refreshes search so pictures from database show up
                refreshSearch(false);
                createAlert("Download successful", "The picture folder has been updated with pictures from the database", Alert.AlertType.INFORMATION);
            } catch (SQLException ex) {
                createAlert("Data update failed", "Failed update data from database, SQL Exception", Alert.AlertType.ERROR);
                ex.printStackTrace();
            }
            catch (IOException ex)
            {
                if (ex instanceof FileNotFoundException)
                {
                    createAlert("Data update failed", "Failed to update data from database, File not found exception", Alert.AlertType.ERROR);
                }
                else {
                    createAlert("Data update failed", "Failed update data from database, IO Exception", Alert.AlertType.ERROR);

                }
                ex.printStackTrace();
            }
        });
        Button pushDBButton = createToolBarButton("icons/cloud_push.png",25,25);
        pushDBButton.setTooltip(new Tooltip("Some local images have not been saved to the database"));
        pushDBButton.setVisible(false);
        pushDBButton.setOnAction(e->{
            try {
                fileManager.uploadDataToDB();
                updateSearchModifiers();
                // Refreshes search so pictures from database show up
                refreshSearch(false);
                createAlert("Upload successful", "The picture folders contents have been uploaded to the database", Alert.AlertType.INFORMATION);
            } catch (FileNotFoundException ex) {
                createAlert("Error", "File could not be found", Alert.AlertType.ERROR);
                ex.printStackTrace();
            } catch (SQLException ex) {
                if (ex instanceof com.mysql.jdbc.PacketTooBigException)
                {
                    createAlert("Upload failed", "Size of the file you are trying to upload is too big \nPacketTooBigException: increase 'max_allowed_packet_size' in phpMyAdmin", Alert.AlertType.ERROR);
                } else {
                    createAlert("Error", "Something went wrong, an SQL exception occured", Alert.AlertType.ERROR);
                }
                ex.printStackTrace();
            } catch (FileAlreadyExistsException ex)
            {
                createAlert("Upload failed", "The file you are trying to upload already exists in the database", Alert.AlertType.ERROR);
                ex.printStackTrace();
            }
        });
        Button syncDBButton = createToolBarButton("icons/cloud_sync.png",25,25);
        syncDBButton.setTooltip(new Tooltip("Some images are missing from DB and locally"));
        syncDBButton.setVisible(false);
        syncDBButton.setOnAction( e -> {
            try {
                fileManager.DownloadDataFromDB();
                fileManager.uploadDataToDB();
                updateSearchModifiers();
                // Refreshes search so pictures from database show up
                refreshSearch(false);
                createAlert("Download successful", "The picture folder has been updated with pictures from the database", Alert.AlertType.INFORMATION);
            } catch (SQLException ex) {
                createAlert("Data update failed", "Failed update data from database, SQL Exception", Alert.AlertType.ERROR);
                ex.printStackTrace();
            }
            catch (IOException ex)
            {
                if (ex instanceof FileNotFoundException)
                {
                    createAlert("Data update failed", "Failed to update data from database, File not found exception", Alert.AlertType.ERROR);
                }
                else {
                    createAlert("Data update failed", "Failed update data from database, IO Exception", Alert.AlertType.ERROR);

                }
                ex.printStackTrace();
            }

        });
        cloudButtons.getChildren().addAll(upToDateDBButton, pullDBButton, pushDBButton, syncDBButton);



        // Create container for buttons
        HBox toolBar = new HBox();
        HBox.setMargin(cloudButtons, new Insets(0,5,0,0));
        toolBar.setSpacing(5);
        toolBar.getStyleClass().addAll("element");
        toolBar.getChildren().addAll(backButton, editButton, new Separator(Orientation.VERTICAL),addButton,exportButton, new Separator(Orientation.VERTICAL), cloudButtons);

        /* ------------
             FILTERS
           ------------ */

        VBox filterPane = createFilterPane();

        /* ------------
               TAGS
           ------------ */
        tagToggleView = new FlowPane();
        tagToggleView.getStyleClass().add("element");
        tagToggleView.setMaxWidth(leftPaneWidth);
        tagToggleView.setMinHeight(100);
        tagToggleView.setHgap(5);
        tagToggleView.setVgap(5);

        selectedSearchTags = new ArrayList<>();

        //Scrollpane for tagView
        ScrollPane tagScrollPane = new ScrollPane();
        tagScrollPane.setStyle("-fx-background-color: white");
        tagScrollPane.setMinHeight(100);
        tagScrollPane.setMaxHeight(100);
        tagScrollPane.setMinWidth(leftPaneWidth);
        tagScrollPane.setMaxWidth(leftPaneWidth);
        tagScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        tagScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        tagScrollPane.setContent(tagToggleView);

        // Indicator
        GridPane indicatorInfo = new GridPane();
        indicatorInfo.setVgap(2);
        indicatorInfo.setHgap(2);
        Button redSquare = new Button();
        redSquare.setDisable(true);
        redSquare.setMinSize(1,1);
        redSquare.setStyle("-fx-background-color: #FF9298");
        indicatorInfo.add(redSquare,1,1);
        indicatorInfo.add(new Text("Indicates that the image is only saved locally"), 2 ,1);
        Button greenSquare = new Button();
        greenSquare.setDisable(true);
        greenSquare.setMinSize(1,1);
        greenSquare.setStyle("-fx-background-color: #8ECC85");
        indicatorInfo.add(greenSquare,1,2);
        indicatorInfo.add(new Text("Indicates that the image is uploaded to database"), 2 ,2);

        // Create container for all elements in the left pane
        VBox leftPane = new VBox();
        leftPane.setEffect(dropShadow);
        leftPane.setMinSize(leftPaneWidth, defaultHeight);
        leftPane.setSpacing(10);
        leftPane.setPadding(new Insets(10, 5, 10, 10));
        leftPane.setAlignment(Pos.TOP_LEFT);
        leftPane.getChildren().addAll(toolBar, filterPane, tagScrollPane, indicatorInfo);

        /*--------------------------------------------------
                              RIGHT PANE
          -------------------------------------------------- */
        /* ------------
            SEARCH BAR
           ------------ */
        searchField = new TextField(); //For taking search input
        searchField.getStyleClass().addAll("element", "elementText");
        searchField.setPromptText("Search me");
        searchField.setMaxWidth(850);
        searchField.setPrefHeight(25);
        searchField.setOnKeyReleased(e -> {
            // When typing the results are gotten through the fileManager's search() method and then displayed with displaySearch()
            displaySearch(false);
        });

        /* ------------
              GALLERY
           ------------ */
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scrollPane.setMaxHeight(570);
        scrollPane.setMinHeight(570);


        // Create container for elements in right pane
        VBox rightPane = new VBox();
        rightPane.setMinSize(850, defaultHeight);
        rightPane.setSpacing(10);
        rightPane.setPadding(new Insets(10, 10, 10, 5));
        rightPane.setAlignment(Pos.TOP_CENTER);
        rightPane.setEffect(dropShadow);
        rightPane.getChildren().addAll(searchField, scrollPane);

        /* ------------
               ROOT
           ------------ */
        // Create root node
        HBox root = new HBox();
        root.setAlignment(Pos.CENTER);
        root.getStyleClass().add("background");
        root.getChildren().addAll(leftPane, rightPane);

        // Create and return scene
        Scene scene = new Scene(root, defaultWidth, defaultHeight, Color.PEACHPUFF);
        scene.getStylesheets().add(getClass().getResource("style/main.css").toExternalForm());
        return scene;
    }



    /**
     * Method for moving the creating of the filterPane and all its elements outside the main creation method
     * @return a customized VBox containing all elements
     */
    private VBox createFilterPane(){
        //Resolution
        Label resolutionLabel = new Label("Resolution:" + "                                                     " + "2160p+"); //Cheesy
        VBox resolutionContainer = new VBox();
        resolutionSlider = createRangeSlider();
        resolutionContainer.getChildren().addAll(resolutionLabel, resolutionSlider);


        //Aspect Ration
        VBox aspectContainer = new VBox();
        Label aspectLabel = new Label("Resolution: ");
        HBox aspectButtons = new HBox();
        aspectButtons.setSpacing(2);
        landscapeButton = new CheckBox("Landscape");
        landscapeButton.setSelected(true);
        landscapeButton.setOnAction(e -> displaySearch(false));
        portraitButton = new CheckBox("Portrait");
        portraitButton.setSelected(true);
        portraitButton.setOnAction(e -> displaySearch(false));
        aspectButtons.getChildren().addAll(landscapeButton, portraitButton);
        aspectContainer.getChildren().addAll(aspectLabel, aspectButtons);

        //File size (Kilobytes)
        VBox fileSizeContainer = new VBox();
        fileSizeLabel = new Label("File size: ");
        fileSizeSlider = createRangeSlider();
        fileSizeContainer.getChildren().addAll(fileSizeLabel, fileSizeSlider);

        //Focal length (Millimeters)
        VBox focalContainer = new VBox();
        Label focalLabel = new Label("Focal Length (Millimeters): ");
        focalSlider = createRangeSlider();
        focalContainer.getChildren().addAll(focalLabel, focalSlider);

        //Brightness value
        VBox brightnessContainer = new VBox();
        Label brightnessLabel = new Label("Brightness: ");
        brightnessSlider = createRangeSlider();
        brightnessContainer.getChildren().addAll(brightnessLabel, brightnessSlider);

        //ISO
        VBox isoContainer = new VBox();
        Label isoLabel = new Label("ISO: ");
        isoSlider = createRangeSlider();
        isoContainer.getChildren().addAll(isoLabel, isoSlider);

        //Default state
        Button defaultStateButton = new Button("Return to Default");
        defaultStateButton.setOnAction(e -> {
            updateSearchModifiers();
            refreshSearch(false);
        });

        //Create a container for the filter elements
        VBox filterPane = new VBox();
        filterPane.setSpacing(5);
        filterPane.getStyleClass().addAll("element");
        filterPane.setMinSize(leftPaneWidth, 300);
        filterPane.getChildren().add(new Label("Filters: "));
        filterPane.getChildren().add(new Separator());
        filterPane.getChildren().addAll(resolutionContainer, aspectContainer, fileSizeContainer, focalContainer, brightnessContainer, isoContainer, defaultStateButton);

        return filterPane;
    }

    /**
     * Enabled and disables the deletion mode according to the boolean and the state of the add to collection mode
     * @param enabled the state at which the mode was originally
     * @return the current state of this mode
     */
    public boolean searchDeletionMode(boolean enabled) {
        if (enabled)
        {
            // disable deletion of images
            for (Node buttonPane : galleryView.getChildren())
            {
                StackPane stackPane = (StackPane) buttonPane;
                stackPane.getChildren().get(1).setVisible(false);
            }
            enabled = false;
        }
        else {
            // enable deletion of images
            for (Node buttonPane : galleryView.getChildren())
            {
                StackPane stackPane = (StackPane) buttonPane;
                stackPane.getChildren().get(1).setVisible(true);
            }
            enabled = true;
        }

        return enabled;
    }

    /**
     * Enabled and disables the add to collection mode according to the boolean and the state of the deletion mode
     * @param enabled the state at which the mode was originally
     * @return the current state of this mode
     */
    public boolean searchAddToCollectionMode(boolean enabled) {
        if (enabled)
        {
            for (Node buttonPane : galleryView.getChildren())
            {
                StackPane stackPane = (StackPane) buttonPane;
                stackPane.getChildren().get(2).setVisible(false);
            }
            enabled = false;
        }
        else {
            for (Node buttonPane : galleryView.getChildren())
            {
                StackPane stackPane = (StackPane) buttonPane;
                stackPane.getChildren().get(2).setVisible(true);
            }
            enabled = true;
        }

        return enabled;
    }

    /**
     * Updates the file status based on an integer value and shows the appropriate button
     */
    public void updateFileStatus()
    {

        // An icon that changes based on status of files

        int status = 0;

        try {
            // Status of file loading / saving
            status = fileManager.fileStatus();
        } catch (SQLException ex)
        {
            createAlert("Error", "File status update failed", Alert.AlertType.ERROR);
            ex.printStackTrace();
        }

        /*
        status = 3: there are both files in the folder which are not save and files in the database which are not loaded
        status = 2: there are files in the folder which have not been saved to the database
        status = 1: there are files in the database which have not been loaded into the folder yet
        status = 0: everything is saved and upto date
         */

        if(status == 0){
            cloudButtons.getChildren().get(0).setVisible(true);
            cloudButtons.getChildren().get(1).setVisible(false);
            cloudButtons.getChildren().get(2).setVisible(false);
            cloudButtons.getChildren().get(3).setVisible(false);

        } else if (status == 1){
            cloudButtons.getChildren().get(0).setVisible(false);
            cloudButtons.getChildren().get(1).setVisible(true);
            cloudButtons.getChildren().get(2).setVisible(false);
            cloudButtons.getChildren().get(3).setVisible(false);
        } else if(status == 2){
            cloudButtons.getChildren().get(0).setVisible(false);
            cloudButtons.getChildren().get(1).setVisible(false);
            cloudButtons.getChildren().get(2).setVisible(true);
            cloudButtons.getChildren().get(3).setVisible(false);
        } else if(status == 3){
            cloudButtons.getChildren().get(0).setVisible(false);
            cloudButtons.getChildren().get(1).setVisible(false);
            cloudButtons.getChildren().get(2).setVisible(false);
            cloudButtons.getChildren().get(3).setVisible(true);
        }
    }

    /**
     * Updates the search modifiers on the search scene
     */
    public void updateSearchModifiers() {

        updateFileStatus();

        // Checkboxes
        landscapeButton.setSelected(true);
        portraitButton.setSelected(true);

        // Resolution
        resolutionSlider.setMin(0);
        resolutionSlider.setMax(2160);
        resolutionSlider.setHighValue(2160);

        // File size
        fileSizeSlider.setMin(fileManager.getMinFileSize());
        fileSizeSlider.setLowValue(fileManager.getMinFileSize());
        fileSizeSlider.setMax(fileManager.getMaxFileSize());
        fileSizeSlider.setHighValue(fileManager.getMaxFileSize());
        if (fileManager.fileSizeSliderMultiplier())
        {
            fileSizeLabel.setText("File size (Megabytes):");
        }
        else {
            fileSizeLabel.setText("File size (Kilobytes):");
        }

        // ISO
        isoSlider.setMin(fileManager.getMinISO());
        isoSlider.setLowValue(fileManager.getMinISO());
        isoSlider.setMax(fileManager.getMaxISO());
        isoSlider.setHighValue(fileManager.getMaxISO());

        // Brightness
        brightnessSlider.setMin(fileManager.getMinBrightnessValue());
        brightnessSlider.setLowValue(fileManager.getMinBrightnessValue());
        brightnessSlider.setMax(fileManager.getMaxBrightnessValue());
        brightnessSlider.setHighValue(fileManager.getMaxBrightnessValue());

        // Focal length
        focalSlider.setMin(fileManager.getMinFocalLength());
        focalSlider.setLowValue(fileManager.getMinFocalLength());
        focalSlider.setMax(fileManager.getMaxFocalLength());
        focalSlider.setHighValue(fileManager.getMaxFocalLength());

        // search Tags
        selectedSearchTags.clear();
        ArrayList<String> searchTags = fileManager.getSearchTags();
        tagToggleView.getChildren().clear();
        for (String tag : searchTags)
        {
            tagToggleView.getChildren().add(createTagToggleButton(tag));
        }
    }

    /**
     * Displays the search results
     *
     * @param deleteVisible tells if the delete button should be visible
     */
    public void displaySearch(boolean deleteVisible){

        // Sets both tool bar modes to false
        deletionModeEnabled = new AtomicBoolean(false);
        addToCollectionModeEnabled = new AtomicBoolean(false);

        List<Picture> result = fileManager.search(searchField.getText(), landscapeButton.isSelected(), portraitButton.isSelected(), selectedSearchTags, (int) fileSizeSlider.getLowValue(), (int) fileSizeSlider.getHighValue(), (int) resolutionSlider.getLowValue(), (int) resolutionSlider.getHighValue(), (int) focalSlider.getLowValue(), (int) focalSlider.getHighValue(), (int) brightnessSlider.getLowValue(), (int) brightnessSlider.getHighValue(), (int) isoSlider.getLowValue(), (int) isoSlider.getHighValue());
        // If result list is not empty
        galleryView = new TilePane(); //For displaying search results

        // easter egg
        if(searchField.getText().equalsIgnoreCase("Bomb recipe")){
            searchField.clear();
            VBox root = new VBox();
            root.setStyle("-fx-background-image: url(https://wallpapercave.com/wp/lHaEDlj.jpg);\n" +
                          "-fx-background-position: center center;\n");
            Scene scene =new Scene(root, defaultWidth, defaultHeight);
            scene.setCursor(Cursor.HAND);
            scene.setOnMouseClicked( e-> primaryStage.setScene(mainScene));
            primaryStage.setScene(scene);
        }

        // Checks if any pictures were found
        if (!result.isEmpty()) {
            galleryView = new TilePane();
            galleryView.setMinWidth(850);
            galleryView.setMinHeight(570);
            galleryView.setVgap(7);
            galleryView.setHgap(7);
            galleryView.getStyleClass().addAll("element", "elementText");
            galleryView.setPadding(new Insets(0, 0, 0, 0));

            for (Picture image : result) {

                Button button = new Button("");
                button.setMinSize(150, 150);
                String path = image.getPath();
                button.setStyle("-fx-background-image: url('" + path + "')");
                button.getStyleClass().add("pictureButton");
                button.setOnAction(e -> {
                    updatePreview(image, searchScene);
                    primaryStage.setScene(previewScene);
                });

                // Stack pane for buttons
                StackPane stackPane = new StackPane();
                stackPane.getStyleClass().add("stackPane");

                // Delete picture button
                Button delete = new Button("");
                delete.setTooltip(new Tooltip("Delete"));
                delete.setStyle("-fx-background-image: url(icons/trash.png)");
                delete.getStyleClass().addAll("overlayButton");
                delete.setMinSize(30, 30);
                delete.setMaxSize(30, 30);
                delete.setOnAction(e -> {
                    // Deletes file
                    fileManager.deleteFile(image);
                    // Refreshes search
                    refreshSearch(true);
                    // Sets deletion mode appropriately so it functions correctly
                    deletionModeEnabled.set(true);
                    // Updates search modifiers
                    updateSearchModifiers();

                });
                delete.setVisible(deleteVisible);

                // Add to collection picture button
                Button addButton = new Button("");
                addButton.setTooltip(new Tooltip("Select"));
                if (image.getPDFExport())
                {
                    addButton.setStyle("-fx-background-image: url(icons/cancel.png)");
                }
                else {
                    addButton.setStyle("-fx-background-image: url(icons/add_image.png)");
                }

                addButton.getStyleClass().addAll("overlayButton");
                addButton.setMinSize(30, 30);
                addButton.setMaxSize(30, 30);
                addButton.setOnAction(e -> {
                    if (image.getPDFExport()) // Checks if image is added or not
                    {
                        // Deletes image from pdf export collection
                        addButton.setStyle("-fx-background-image: url(icons/add_image.png)");
                        //addButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("UI/add_image.png"), 10, 10, false, false)));
                        image.setPDFExport(false);
                    } else {
                        // Adds image to pdf export collection
                        addButton.setStyle("-fx-background-image: url(icons/cancel.png)");
                        //addButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("UI/cancel.png"), 10, 10, false, false)));
                        image.setPDFExport(true);
                    }

                });
                addButton.setVisible(false);

                stackPane.setAlignment(Pos.TOP_RIGHT);
                stackPane.getChildren().addAll(button, delete, addButton);
                if(image.getPictureID() == -1){
                    stackPane.setStyle("-fx-padding: 4px; -fx-background-color: #FF9298");
                } else {
                    stackPane.setStyle("-fx-padding: 4px; -fx-background-color: #8ecc85");
                }

                // Adds stacked pane to gallery view
                galleryView.getChildren().add(stackPane);

                button.setPadding(new Insets(0, 0, 0, 0));
            }
        } // If result list is empty
        else {

            Button button = new Button("No Results Found");
            button.setMinSize(150, 150);
            button.getStyleClass().add("pictureButton");
            button.setDisable(true);

            StackPane stackPane = new StackPane();
            stackPane.getStyleClass().add("stackPane");
            stackPane.setStyle("-fx-padding: 4px; -fx-background-color: white");
            stackPane.getChildren().add(button);

            galleryView.getChildren().add(stackPane);
            galleryView.setMinWidth(850);
            galleryView.setMinHeight(570);
            galleryView.setVgap(7);
            galleryView.setHgap(7);
            galleryView.getStyleClass().addAll("element");
            galleryView.setPadding(new Insets(0, 0, 0, 0));

        }
        scrollPane.setContent(galleryView);
    }

    /**
     * Method for refreshing the search results
     */
    public void refreshSearch(boolean deleteVisible) {
        displaySearch(deleteVisible);
    }

    /*|---------------------------------------------------------------------------------|
                                        PREVIEW SCENE
      |---------------------------------------------------------------------------------|*/

    // Raid is particularly effective against pesky bugs, maybe not this kind though

    // ------- Preview scene UI elements -------
    private VBox detailsPane;
    private VBox tagView = new VBox();
    private Label nameView = new Label();
    private AtomicBoolean tagEditEnabled;
    private FlowPane tags = new FlowPane();
    private VBox previewLeftPane = new VBox();
    private VBox confirmationBox = new VBox();
    private VBox previewRightPane = new VBox();
    private Button tagEditButton = new Button();
    private ImageView imageView = new ImageView();
    private ScrollPane scrollPane = new ScrollPane();
    private Button cancel = new Button("Cancel");
    private Button confirm = new Button("Confirm");
    private Button previewBackButton = createButtonWithImage("Back", "icons/back_button.png", 25, 25);


    TextField newTag = new TextField();
    Button commitTag = new Button("Done");
    HBox newTagContainer = new HBox();

    /**
     * Method used for creating a preview scene
     *
     * @return a finished scene
     */
    private Scene setPreviewScene() {

        /*--------------------------------------------------
                              Left Pane
          -------------------------------------------------- */
        /* ------------
            BACK BUTTON
           ------------ */
        previewBackButton.getStyleClass().add("backButton");

        /* ------------
              DETAILS
           ------------ */

        detailsPane = new VBox();
        detailsPane.getStyleClass().addAll("element", "elementText");
        detailsPane.setMinSize(leftPaneWidth, 300);
        detailsPane.getChildren().add(new Label("Details: "));
        detailsPane.getChildren().add(new Separator());

        ScrollPane detailsScrollPane = new ScrollPane();
        detailsScrollPane.setMinHeight(300);
        detailsScrollPane.setContent(detailsPane);
        detailsScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        /* ------------
             TAG VIEW
           ------------ */
        //Elements for adding new tags
        newTagContainer.setVisible(false);
        newTagContainer.setSpacing(5);
        newTagContainer.getChildren().addAll(new Label("Add tag: "), newTag, commitTag);

        tagEditEnabled = new AtomicBoolean(false);
        tagEditButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("icons/edit.png"), 15, 15, false, false)));
        tagEditButton.setTooltip(new Tooltip("Edit mode"));
        tagEditButton.getStyleClass().add("genericButton");
        tagEditButton.setAlignment(Pos.TOP_RIGHT);
        tagEditButton.setOnAction(e -> {
            if (tagEditEnabled.get()) {
                HBox hbox;
                for (Node tag : tags.getChildren()) {
                    hbox = (HBox) tag;
                    for (Node node : hbox.getChildren()) {
                        if (node instanceof Button) {
                            node.setVisible(false);
                        }
                    }
                }

                // Tag add button visiblity
                newTagContainer.setVisible(false);
                tagEditEnabled.set(false);
            } else {
                HBox hbox;
                for (Node tag : tags.getChildren()) {
                    hbox = (HBox) tag;
                    for (Node node : hbox.getChildren()) {
                        if (node instanceof Button) {
                            node.setVisible(true);
                        }
                    }
                }
                // Tag add button visiblity
                newTagContainer.setVisible(true);
                tagEditEnabled.set(true);
            }

        });

        // Container for edit elements
        HBox editTagElements = new HBox();
        editTagElements.setAlignment(Pos.CENTER_RIGHT);
        editTagElements.setSpacing(5);
        editTagElements.getChildren().addAll(newTagContainer, tagEditButton);

        tags.setHgap(10);
        tags.setVgap(10);
        tags.setPadding(new Insets(5,5,5,5));
        tags.setStyle("-fx-background-color: white; -fx-border-color: white");

        ScrollPane tagsScrollPane = new ScrollPane();
        tagsScrollPane.setContent(tags);
        tagsScrollPane.setStyle("-fx-background-color: white; -fx-border-color: white");
        tagsScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        tagsScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

        // Create container for tag elements
        tagView.getStyleClass().addAll("element");
        tagView.setSpacing(5);
        tagView.setMinHeight(200);
        tagView.setMinWidth(leftPaneWidth);
        tagView.getChildren().addAll(editTagElements, tagsScrollPane);

        // "A user interface is like a joke. If you have to explain it, it's not that good.

        /* ------------
            CONFIRM BOX
           ------------ */
        Text text = new Text("Do you want to upload this picture?");

        cancel.getStyleClass().add("cancelButton");
        confirm.getStyleClass().add("confirmButton");

        //Container for buttons
        HBox buttonContainer = new HBox();
        buttonContainer.setAlignment(Pos.CENTER);
        buttonContainer.setSpacing(10);
        buttonContainer.getChildren().addAll(confirm, cancel);

        //Container for the button container and text
        confirmationBox.setMinHeight(100);
        confirmationBox.setAlignment(Pos.CENTER);
        confirmationBox.setMinWidth(leftPaneWidth);
        confirmationBox.setSpacing(10);
        confirmationBox.setStyle("-fx-font-size: 18px");
        confirmationBox.setVisible(false);
        confirmationBox.getChildren().addAll(text,buttonContainer);

        // Create container for elements
        previewLeftPane.setMinSize(leftPaneWidth, defaultHeight);
        previewLeftPane.setEffect(dropShadow);
        previewLeftPane.setSpacing(10);
        previewLeftPane.setPadding(new Insets(10, 5, 10, 10));
        previewLeftPane.getChildren().addAll(previewBackButton, detailsScrollPane, tagView, confirmationBox);

        /*--------------------------------------------------
                               Right Pane
          --------------------------------------------------*/

        /* ------------
            NAME VIEW
           ------------ */
        HBox nameViewContainer = new HBox();
        TextField newName = new TextField();
        Button commitName = new Button("Done");
        commitName.setOnAction(e -> {
            String oldName = nameView.getText();
            String extension = oldName.substring(oldName.indexOf("."));
            nameView.setText(newName.getText() + extension);
            newName.setText("");
            nameViewContainer.getChildren().set(1, nameView);
        });

        /* Incomplete feature
        HBox newNameContainer = new HBox();
        newNameContainer.setSpacing(5);
        newNameContainer.getChildren().addAll(newName, commitName);

        Button editName = new Button();
        editName.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("UI/edit.png"), 15, 15, false, false)));
        editName.getStyleClass().add("genericButton");
        editName.setOnAction(e -> nameViewContainer.getChildren().set(1, newNameContainer));

         */

        nameView.setAlignment(Pos.CENTER);
        nameView.getStyleClass().add("elementText");

        //Create container for nameview

        nameViewContainer.setAlignment(Pos.CENTER);
        nameViewContainer.getStyleClass().add("element");
        nameViewContainer.getChildren().addAll(nameView);
        nameViewContainer.setMinWidth(rightPaneWidth);
        nameViewContainer.setMaxWidth(rightPaneWidth);

        /* ------------
            IMAGE VIEW
           ------------ */
        imageView.getStyleClass().addAll("element", "elementText");

        // Create container for elements
        previewRightPane.setMinSize(rightPaneWidth, defaultHeight);
        previewRightPane.setSpacing(10);
        previewRightPane.setEffect(dropShadow);
        previewRightPane.setPadding(new Insets(10, 10, 10, 5));
        previewRightPane.setAlignment(Pos.TOP_CENTER);
        previewRightPane.getChildren().addAll(nameViewContainer, imageView);

        /* ------------
               ROOT
           ------------ */
        // Create root node
        HBox root = new HBox();
        root.setAlignment(Pos.TOP_CENTER);
        root.getStyleClass().add("background");
        root.getChildren().addAll(previewLeftPane, previewRightPane);

        Scene scene = new Scene(root, defaultWidth, defaultHeight, Color.PEACHPUFF);
        scene.getStylesheets().add(getClass().getResource("style/main.css").toExternalForm());
        return scene;
    }

    /**
     * Updates the picture of the preview scene as well as the back buttons scene destination
     *
     * @param image         new image for the scene
     * @param previousScene the scene which the back button will lead to
     */
    private void updatePreview(Picture image, Scene previousScene) {

        // Adds image, title and button
        nameView.setText(image.getName());

        imageView.setImage(new Image(image.getPath(), rightPaneWidth, 500, true, false));
        // Back button
        previewBackButton.setOnAction(e -> {
            primaryStage.setScene(previousScene);
            deletionModeEnabled.set(false);
            addToCollectionModeEnabled.set(false);
            displaySearch(false);
            confirmationBox.setVisible(false);
        });

        //Why was the developer always tired?
        //He couldn't C# enough to find his java.

        // add tag button
        commitTag.setOnAction(e -> {
            fileManager.addTag(image, newTag.getText());
            newTag.setText("");
            updatePreviewTags(true, image);
            updateSearchModifiers();
        });

        // Updates tags
        updatePreviewTags(false, image);

        // Clears details
        detailsPane.getChildren().clear();
        // Sets metadata details
        Label resolution = new Label("Resolution: " + image.getResolution());
        Label fileSize = new Label("File Size: " + bytesToString(image.getFileSize()));
        Label timeLoaded = new Label("Image last loaded: " + image.getTimeLoadedToString());
        detailsPane.getChildren().addAll(createTextWithPictureIcon("icons/aspect_ratio.png", resolution),
                fileSize,
                createTextWithPictureIcon("icons/timecode.png", timeLoaded));

        // If image is of Model.CameraPicture subclass meaning it contains more details from EXIF data
        if (image instanceof CameraPicture) {
            CameraPicture cameraImage = (CameraPicture) image;

            // checks if time taken is defined
            if (cameraImage.getTimeTaken() != null)
            {
                // Adds to detailPane
                Label timeTaken = new Label("Time taken: " + cameraImage.getTimeTakenToString());
                detailsPane.getChildren().add(createTextWithPictureIcon("icons/timecode.png", timeTaken));
            }

            // checks if color space is defined
            if (cameraImage.getColorSpace() != null)
            {
                // Adds to detailPane
                Label colorSpace = new Label("Color space: " + cameraImage.getColorSpace());
                detailsPane.getChildren().add(createTextWithPictureIcon("icons/info.png", colorSpace));
            }

            // checks if focal length is defined
            if (cameraImage.getFocalLength() != 0)
            {
                // Adds to detailPane
                Label focalLength = new Label("Focal Length: " + cameraImage.getFocalLength() + " mm");
                detailsPane.getChildren().add(createTextWithPictureIcon("icons/focal_length.png", focalLength));
            }

            // Checks if aperature value is defined
            if (cameraImage.getApertureValue() != null)
            {
                // Adds to detailPane
                Label aperture = new Label("Aperture value: " + cameraImage.getApertureValue());
                detailsPane.getChildren().add(createTextWithPictureIcon("icons/aperture.png", aperture));
            }

            // Checks if exposure time is defined
            if(cameraImage.getExposureTime() != null)
            {
                // Adds to detailPane
                Label exposureTime = new Label("Exposure time: " + cameraImage.getExposureTime());
                detailsPane.getChildren().add(createTextWithPictureIcon("icons/exposure.png", exposureTime));
            }

            // Checks if exposure mode is defined
            if (cameraImage.getExposureMode() != null)
            {
                // Adds to detailPane
                Label exposureMode = new Label("Exposure mode: " + cameraImage.getExposureMode());
                detailsPane.getChildren().add(createTextWithPictureIcon("icons/info.png", exposureMode));
            }

            // Checks if iso is defined
            if (cameraImage.getIso() != 0)
            {
                // Adds to detailPane
                Label iso = new Label("ISO: " + cameraImage.getIso());
                detailsPane.getChildren().add(createTextWithPictureIcon("icons/iso.png", iso));
            }

            if (cameraImage.getBrightnessValue() != 0)
            {
                // Adds to detailPane
                Label brightness = new Label("Brightness value: " + cameraImage.getBrightnessValue());
                detailsPane.getChildren().add(createTextWithPictureIcon("icons/info.png", brightness));
            }

            // Checks if flash is defined
            if (cameraImage.getFlash() != null)
            {
                // Adds to detailPane
                Label flash = new Label("Flash: " + cameraImage.getFlash());
                detailsPane.getChildren().add(createTextWithPictureIcon("icons/info.png", flash));
            }

            // Checks if make is defined
            if (cameraImage.getMake() != null)
            {
                // Adds to detailPane
                Label make = new Label("Make: " + cameraImage.getMake());
                detailsPane.getChildren().add(createTextWithPictureIcon("icons/info.png", make));
            }

            // Checks if model is defined
            if (cameraImage.getModel() != null)
            {
                // Adds to detailPane
                Label model = new Label("Model: " + cameraImage.getModel());
                detailsPane.getChildren().add(createTextWithPictureIcon("icons/info.png", model));
            }

            // Checks if software is defined
            if (cameraImage.getSoftware() != null)
            {
                // Adds to detailPane
                Label software = new Label("Software: " + cameraImage.getSoftware());
                detailsPane.getChildren().add(createTextWithPictureIcon("icons/info.png", software));
            }

            // Checks if artist is defined
            if (cameraImage.getArtist() != null)
            {
                // Adds to detailPane
                Label artist = new Label("Artist: " + cameraImage.getArtist());
                detailsPane.getChildren().add(createTextWithPictureIcon("icons/info.png", artist));
            }
        }
    }


    /**
     * Updates the tags shown in the preview scene
     *
     * @param image the image of which to update the tags for
     */
    public void updatePreviewTags(boolean editVisible, Picture image) {
        // Clears the tags showing
        tags.getChildren().clear();

        if (image.getPictureID() != -1)
        {
            tags.setAlignment(Pos.TOP_LEFT);
            // Checks if tagList is empty
            if (image.getTagList().size() > 0) {
                //Tag view
                tagEditButton.setVisible(true);
                newTagContainer.setVisible(editVisible);
                newTag.setText("");
                tagEditEnabled.set(editVisible);
                for (String tag : image.getTagList()) {
                    tags.getChildren().addAll(createTag(editVisible, image, tag));
                }
                tags.setVisible(true);
            } else {
                tags.setVisible(false);
            }
        } else {
            newTagContainer.setVisible(false);
            Button uploadFile = new Button("Upload file to database");
            uploadFile.setOnAction(e -> {
                try {
                    File file = new File(fileManager.getCurrentDir() + image.getName());
                    fileManager.uploadFileToDB(file);
                    updateSearchModifiers();
                    updatePreview(image, searchScene);
                    createAlert("Upload successfull", "The file has been uploaded ", Alert.AlertType.INFORMATION);
                } catch (IOException ex) {
                    if (ex instanceof  FileNotFoundException)
                    {
                        createAlert("Error", "Could not locate file", Alert.AlertType.ERROR);
                    }
                    else {
                        createAlert("Error", "The file you are trying to upload already exists in the database", Alert.AlertType.ERROR);
                    }
                    ex.printStackTrace();
                } catch (SQLException ex) {
                    if (ex instanceof com.mysql.jdbc.PacketTooBigException)
                    {
                        createAlert("Error", "Size of the file you are trying to upload is too big \nPacketTooBigException: increase 'max_allowed_packet_size' in phpMyAdmin", Alert.AlertType.ERROR);
                    } else {
                        createAlert("Error", "Could not upload file to database, SQL exception", Alert.AlertType.ERROR);
                    }
                    ex.printStackTrace();
                }
            });
            Text infoText = new Text("Looks like this image doesn't exist in the database, \n"
                    + "please ensure that it's uploaded before using this feature");
            infoText.setTextAlignment(TextAlignment.CENTER);
            VBox actionBox = new VBox();
            actionBox.setSpacing(5);
            actionBox.setAlignment(Pos.CENTER);
            actionBox.getChildren().addAll(infoText, uploadFile);
            tags.setAlignment(Pos.CENTER);

            tags.setVisible(true);
            tags.getChildren().add(actionBox);
        }


    }

    /**
     * Takes in a long parameter assumed to be bytes and formulates them to a readable form, examples: 102 B, 2.3MiB
     *
     * @param bytes the amount of bytes to be formulated
     * @return formulated byte string
     */
    public static String bytesToString(long bytes) {
        //How do you know if your code is delicious?
        //Try a byte
        if (bytes < 1024) {
            return bytes + " Bytes";
        } else if (bytes / 1024 < 1024) {
            return bytes / 1024 + " Kilobytes";
        } else {
            return Math.round(((double) bytes / (1024 * 1024)) * 100.00) / 100.00 + " Megabytes";
        }
    }

    /*|---------------------------------------------------------------------------------|
                             MISC METHODS FOR CREATING ELEMENTS
      |---------------------------------------------------------------------------------|*/

    /**
     * Method for creating a tag button that can be toggled
     * @param tagName the name of the tag
     * @return a ToggleButton with the name and functionality of a tag
     */
    private ToggleButton createTagToggleButton(String tagName){
        ToggleButton toggleButton = new ToggleButton(tagName);
        toggleButton.getStyleClass().add("tag");
        toggleButton.setOnAction(e->{
            if(toggleButton.isSelected()){
                selectedSearchTags.add(tagName);
            }else {
                selectedSearchTags.remove(tagName);
            }
            displaySearch(false);
        });
        return toggleButton;
    }

    /**
     * Creates tool bar button
     * @param buttonImagePath image of button
     * @param imageWidth width of button
     * @param imageHeight height of button
     * @return tool bar button
     */
    public Button createToolBarButton(String buttonImagePath, int imageWidth, int imageHeight){
        Button button = new Button();
        button.getStyleClass().add("toolBarButton");
        button.setPadding(new Insets(5,5,5,5));
        button.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("" + buttonImagePath), imageWidth, imageHeight, false, false)));
        button.setStyle("-fx-background-radius: 100%");
        return button;
    }

    /**
     * Method for creating a button with a Pop over element
     * @param popOverTitle title of the pop over
     * @param buttonImagePath image displayed on the button
     * @param contentNode content displayed inside the pop over
     * @return a button with a popover element
     */
    private Button createButtonWithPopOver(String buttonName, String popOverTitle, String buttonImagePath, Node contentNode){
        PopOver popOver = new PopOver();
        popOver.setTitle(popOverTitle);
        popOver.setArrowLocation(PopOver.ArrowLocation.BOTTOM_CENTER);
        contentNode.getStyleClass().add("contentNode");
        popOver.setContentNode(contentNode);
        Button button = new Button();
        button.setTooltip(new Tooltip(buttonName));
        button.setPadding(new Insets(5,5,5,5));
        button.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("" + buttonImagePath), 45, 45, false, false)));
        button.setStyle("-fx-background-radius: 100%");
        button.setOnAction(e -> popOver.show(button));

        //Q: How many software engineers does it take to change a light bulb?
        //A: Not our problem; it's a hardware issue.
        return button;
    }

    /**
     * Method for creating a path buttons, used to move from one scene to another.
     *
     * @param buttonName    text that will be displayed on the button
     * @param imagePathName name of the image that will be displayed on the button
     * @param width         width of the image
     * @param height        height of the image
     * @return a working button with a height, width and image that when clicked, changes the scene to another
     */
    public Button createButtonWithImage(String buttonName, String imagePathName, int width, int height) {
        Button button = new Button(buttonName);
        button.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("" + imagePathName), width, height, false, false)));
        return button;
    }

    /**
     * Method for creating a RangeSlider with desired values
     *
     * @return a customized range slider
     */
    private RangeSlider createRangeSlider() {
        RangeSlider rs = new RangeSlider(0, 0, 0, 0);
        rs.setShowTickMarks(true);
        rs.setShowTickLabels(true);
        rs.setBlockIncrement(10);
        rs.setSnapToTicks(true);
        rs.setOnMouseReleased(e -> displaySearch(false));
        return rs;
    }

    /**
     * Method for creating an alert message, used to convey information to the user
     *
     * @param alertTitle title of the alert
     * @param alertMessage alert message
     * @param alertType alert type
     */
    private void createAlert(String alertTitle, String alertMessage, Alert.AlertType alertType) {
        Alert alert = new Alert(alertType);
        alert.setTitle("Feedback System");
        alert.setHeaderText(alertTitle);
        alert.setContentText(alertMessage);
        alert.showAndWait();
    }

    /**
     * Method used for creating a text element with an icon in front
     *
     * @param path  path to the picture that is to be displayed
     * @param label what the text element will show
     * @return HBox with correct elements
     */
    private HBox createTextWithPictureIcon(String path, Label label) {
        HBox hbox = new HBox();
        hbox.getChildren().addAll(new ImageView(new Image(path, 25, 25, true, false)), label);
        return hbox;
    }

    /**
     * Method for creating a tag
     *
     * @param name the name of the tag
     * @return a HBox with all the necessary elements
     */
    private HBox createTag(boolean deleteVisible, Picture image, String name) {
        HBox tag = new HBox();
        tag.setPadding(new Insets(5, 5, 5, 5));
        tag.getStyleClass().add("tag");
        tag.setStyle("-fx-background-color: #ffae4e");

        Label tagName = new Label(name);
        tagName.setStyle("-fx-font-size: 16");

        Button deleteButton = new Button();
        deleteButton.setTooltip(new Tooltip("Delete"));
        deleteButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("icons/trash.png"), 10, 10, false, false)));
        deleteButton.setVisible(deleteVisible);
        deleteButton.getStyleClass().add("genericButton");
        deleteButton.setOnAction(e -> {
            // Delete tag
            fileManager.deleteTag(image, name);
            updatePreviewTags(true, image);
            updateSearchModifiers();
        });

        tag.getChildren().addAll(tagName, deleteButton);
        return tag;
    }
}

