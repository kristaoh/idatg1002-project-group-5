package Model;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.MetadataException;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.file.FileSystemDirectory;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * Responsible for reading the metadata of a picture
 *
 * @author IDATG1002 Group 5
 */
public class Metadata {

    private static Metadata instance = null;

    private com.drew.metadata.Metadata metadata;
    private ExifSubIFDDirectory exifDirectory;
    private FileSystemDirectory fileSysDirectory;

    private Metadata() {

    }

    /**
     * Gets instance of Model.Metadata class
     * @param file file to be read by the class
     * @return instance of Model.Metadata class
     */
    public static Metadata getInstance(File file) {
        // If the instance does not exist yet
        if (instance == null) {
            instance = new Metadata();

        }

        // Attempts to load in the metadata for the specific file
        try {
            instance.getFileMetadata(file);
        } catch (ImageProcessingException ex) {
            System.out.println("Image processing exception");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("IO exception");
            ex.printStackTrace();
        }

        return instance;
    }

    /**
     * Gets the metadata of the file
     * @param file file to get metadata from
     * @throws ImageProcessingException Image processing exception
     * @throws IOException IO exception
     */
    public void getFileMetadata(File file) throws ImageProcessingException, IOException {
        metadata = ImageMetadataReader.readMetadata(file);
        exifDirectory = getEXIFDir();
        fileSysDirectory = getFileSysDir();
    }

    /**
     * Gets the file size if available
     *
     * @return file size in bytes
     */
    public long getFileSize() {
        try {
            return fileSysDirectory.getLong(FileSystemDirectory.TAG_FILE_SIZE);
        } catch (MetadataException ex) {
            System.out.println("Model.Metadata exception");
            ex.printStackTrace();
        }
        return 0;
    }

    /**
     * Gets the date time taken if available
     *
     * @return date time taken
     */
    public Date getDateTaken() {
        if (exifDirectory.containsTag(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL)) {
            return exifDirectory.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL);
        }
        return null;
    }

    /**
     * Gets the focal length if available
     *
     * @return focal length
     */
    public double getFocalLength() {
        if (exifDirectory.containsTag(ExifSubIFDDirectory.TAG_FOCAL_LENGTH)) {
            try {
                return exifDirectory.getDouble(ExifSubIFDDirectory.TAG_FOCAL_LENGTH);
            } catch (MetadataException ex) {
                ex.printStackTrace();
            }

        }
        return 0;
    }

    /**
     * Gets the ISO speed rating if available
     *
     * @return ISO speed rating
     */
    public int getISO() {
        try {
            // for some reason ExifSubIFDDirectory.TAG_ISO_SPEED does not work
            if (exifDirectory.containsTag(34855)) {
                return exifDirectory.getInt(34855);
            }
        } catch (MetadataException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    /**
     * Gets the aperture value if available
     *
     * @return aperture value
     */
    public String getApertureValue() {
        if (exifDirectory.containsTag(ExifSubIFDDirectory.TAG_APERTURE)) {
            return exifDirectory.getDescription(ExifSubIFDDirectory.TAG_APERTURE);
        }
        return null;
    }

    /**
     * Gets the exposure time if available
     *
     * @return exposure time
     */
    public String getExposureTime() {
        if (exifDirectory.containsTag(ExifSubIFDDirectory.TAG_EXPOSURE_TIME)) {
            return exifDirectory.getDescription(ExifSubIFDDirectory.TAG_EXPOSURE_TIME);
        }
        return null;
    }

    /**
     * Gets the make of the camera
     *
     * @return make of the camera
     */
    public String getMake() {
        if (exifDirectory.containsTag(ExifSubIFDDirectory.TAG_MAKE)) {
            return exifDirectory.getDescription(ExifSubIFDDirectory.TAG_MAKE);
        }
        return null;
    }

    /**
     * Gets the model of the camera
     *
     * @return model of the camera
     */
    public String getModel() {
        if (exifDirectory.containsTag(ExifSubIFDDirectory.TAG_MODEL)) {
            return exifDirectory.getDescription(ExifSubIFDDirectory.TAG_MODEL);
        }
        return null;
    }

    /**
     * Gets exposure mode
     *
     * @return exposure mode
     */
    public String getExposureMode() {
        if (exifDirectory.containsTag(ExifSubIFDDirectory.TAG_EXPOSURE_MODE)) {
            return exifDirectory.getDescription(ExifSubIFDDirectory.TAG_EXPOSURE_MODE);
        }
        return null;
    }

    /**
     * Gets the shutter speed
     *
     * @return shutter speed
     */
    public String getShutterSpeedValue() {
        if (exifDirectory.containsTag(ExifSubIFDDirectory.TAG_SHUTTER_SPEED)) {
            return exifDirectory.getDescription(ExifSubIFDDirectory.TAG_SHUTTER_SPEED);
        }
        return null;
    }

    /**
     * Gets the software the picture was put through
     *
     * @return the software
     */
    public String getSoftware() {
        if (exifDirectory.containsTag(ExifSubIFDDirectory.TAG_SOFTWARE)) {
            return exifDirectory.getDescription(ExifSubIFDDirectory.TAG_SOFTWARE);
        }
        return null;
    }

    /**
     * Gets the artist of the picture
     *
     * @return artist
     */
    public String getArtist() {
        if (exifDirectory.containsTag(ExifSubIFDDirectory.TAG_ARTIST)) {
            return exifDirectory.getDescription(ExifSubIFDDirectory.TAG_ARTIST);
        }
        return null;
    }

    /**
     * Gets if flash was used
     *
     * @return if flash was used
     */
    public String getFlash() {
        if (exifDirectory.containsTag(ExifSubIFDDirectory.TAG_FLASH)) {
            return exifDirectory.getDescription(ExifSubIFDDirectory.TAG_FLASH);
        }
        return null;
    }

    /**
     * Gets the brightness value
     *
     * @return brightness value
     */
    public double getBrightnessValue() {

        try {
            if (exifDirectory.containsTag(ExifSubIFDDirectory.TAG_BRIGHTNESS_VALUE)) {
                return exifDirectory.getDouble(ExifSubIFDDirectory.TAG_BRIGHTNESS_VALUE);
            }
        } catch (MetadataException ex) {
            System.out.println("Model.Metadata exception");
            ex.printStackTrace();
        }
        return 0;
    }

    /**
     * Gets the color space
     *
     * @return color space
     */
    public String getColorSpace() {

        if (exifDirectory.containsTag(ExifSubIFDDirectory.TAG_COLOR_SPACE)) {
            return exifDirectory.getDescription(ExifSubIFDDirectory.TAG_COLOR_SPACE);
        }
        return null;
    }

    /**
     * Gets the EXIF Data Directory
     *
     * @return EXIF data directory
     */
    public ExifSubIFDDirectory getEXIFDir() {
        return metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
    }

    /**
     * Gets the File System Directory
     *
     * @return File System directory
     */
    public FileSystemDirectory getFileSysDir() {
        return metadata.getFirstDirectoryOfType(FileSystemDirectory.class);
    }


}