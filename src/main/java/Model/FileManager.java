package Model;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.*;
import java.nio.file.*;
import java.sql.*;
import java.util.*;
import java.util.List;

/**
 * Responsible for database and file handling
 *
 * @author IDATG1002 Group 5
 */
public class FileManager {

    private final static long megabyte = 1024*1024;
    private final static long kilobyte = 1024;
    private final static int fileSizeSliderLimit = 7; // In Megabytes (Slider changes from Kilobytes to Megabytes if a file > 5 mb is found)

    private double uploadSizeLimit;

    private PictureFactory pictureFactory;
    private ArrayList<Picture> files;
    private ArrayList<String> allowedFormats;
    private Connection conn;
    private String username;
    private String password;
    private File dir = new File("");

    public FileManager() {
        pictureFactory = PictureFactory.getInstance();
        files = new ArrayList<>();
        allowedFormats = new ArrayList<>();
        allowedFormats.add("jpg");
        allowedFormats.add("jpeg");
        allowedFormats.add("png");
        allowedFormats.add("gif");
        allowedFormats.add("ico");
        allowedFormats.add("bmp");
        //allowedFormats.add("svg");
        //allowedFormats.add("tiff");
        conn = null;
    }



    /*|---------------------------------|
              DATABASE CONNECTION
      |---------------------------------|*/

    /**
     * Attempts to make a connection with the database based on login parameters
     *
     * @return the database Connection (object)
     */
    private Connection getConnection() throws SQLException {
        // Connection data
        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://mysql-ait.stud.idi.ntnu.no:3306/" + username + "?useSSL=false";

        // Testing / local database
        /*
        String dbname = "";
        String url = "jdbc:mysql://localhost:3306/" + dbname;
        String username = "root";
        String password = "";
         */

        Connection conn = null;
        try {
            Class.forName(driver);
            // Creates connection
            conn = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException ex)
            {
            ex.printStackTrace();
        }

        // returns Connection object
        return conn;
    }

    /**
     * Creates the required database structure in order for the application to work, if the structure does not exist already on the current database server
     * @throws SQLException SQL exception
     */
    public void createDBStructureIfNotExistent() throws SQLException
    {
        // Defines the max allowed packet size
        uploadSizeLimit = getMaxAllowedPacketSize();

        // Gets connection
        conn = getConnection();

        String disableFKCheck = "SET FOREIGN_KEY_CHECKS = 0;";
        Statement fkDisable = conn.createStatement();
        fkDisable.execute(disableFKCheck);
        fkDisable.close();

        // Adds directory table
        String sqlCreateDirectory = "CREATE TABLE IF NOT EXISTS directory (\n" +
                "  ID int(11) NOT NULL AUTO_INCREMENT,\n" +
                "  DirPath varchar(500) DEFAULT NULL,\n" +
                "  PRIMARY KEY (ID)\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";
        Statement stmt = conn.createStatement();
        stmt.execute(sqlCreateDirectory);
        stmt.close();

        // Adds picture table
        String sqlCreatePicture = "CREATE TABLE IF NOT EXISTS picture (\n" +
                "  ID int(11) NOT NULL AUTO_INCREMENT,\n" +
                "  FileName varchar(100) DEFAULT NULL,\n" +
                "  Object longblob NOT NULL,\n" +
                "  PRIMARY KEY (ID)\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";
        Statement stmt2 = conn.createStatement();
        stmt2.execute(sqlCreatePicture);
        stmt2.close();

        // Adds tags table
        String sqlCreateTags = "CREATE TABLE IF NOT EXISTS tags (\n" +
                "  ID int(11) NOT NULL AUTO_INCREMENT,\n" +
                "  PictureID int(11) NOT NULL,\n" +
                "  Tag varchar(150) NOT NULL,\n" +
                "  PRIMARY KEY (ID),\n" +
                "  CONSTRAINT PictureTag FOREIGN KEY (PictureID) REFERENCES picture (ID) ON DELETE CASCADE ON UPDATE CASCADE\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";
        Statement stmt3 = conn.createStatement();
        stmt3.execute(sqlCreateTags);
        stmt3.close();


        String enableFKCheck = "SET FOREIGN_KEY_CHECKS = 1;";
        Statement fkEnable = conn.createStatement();
        fkEnable.execute(enableFKCheck);
        fkEnable.close();


        // Closes connection
        conn.close();
    }

    /**
     * Gets the value of the "max_allowed_packet_size" variabel from the MySQL server and writes it in megabytes
     * @return the max allowed packet size value in megabytes
     * @throws SQLException SQL exception
     */
    public double getMaxAllowedPacketSize() throws SQLException {
        // Gets connection
        conn = getConnection();

        String sqlMaxAllowedPacketSize = "show variables like 'max_allowed_packet'";
        Statement stmtMaxAllowedPacketSize = conn.createStatement();

        ResultSet rs =stmtMaxAllowedPacketSize.executeQuery(sqlMaxAllowedPacketSize);
        rs.next();
        double maxAllowedPacketSize = (double) rs.getInt(2) / megabyte;
        rs.close();

        stmtMaxAllowedPacketSize.close();

        // Closes connection
        conn.close();

        // In megabytes
        return maxAllowedPacketSize;
    }

    /**
     * Sets the new database login details
     * @param username new database login username
     * @param password new database login password
     */
    public void setDBLoginDetails(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * Tests the connection to the database
     * @throws SQLException sql exception
     */
    public void testDBConnection() throws SQLException {
        conn = getConnection();

        conn.close();
    }


    /*|---------------------------------------|
          PICTURE DIRECTORY RELATED METHODS
      |---------------------------------------|*/

    /**
     * Checks if a directory path has already been defined in the database
     * @return true or false based on existance of directory path
     */
    public boolean checkForExistingDirectory() {
        try {
            // Gets connection
            conn = getConnection();

            // Gets the directory path from database
            ResultSet rs = conn.createStatement().executeQuery("SELECT DirPath FROM directory");
            if (rs.isBeforeFirst())
            {
                rs.next();
                String dirString = rs.getString(1);

                File dir = new File(dirString);

                rs.close();

                conn.close();

                // Directory exists if true
                return dir.isDirectory();

            }
            else {
                rs.close();

                conn.close();
                // Directory is not set yet
                return false;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Uses the existing directory path found in the database
     * @throws SQLException sql exception
     */
    public void useExisitingDirectory() throws SQLException {

        // Gets connection
        conn = getConnection();

        // Finds the available Tags and their PictureID
        ResultSet rs = conn.createStatement().executeQuery("SELECT DirPath FROM directory");
        rs.next();
        String dirString = rs.getString(1);

        File dir = new File(dirString);

        setNewDir(dir);

        rs.close();

        conn.close();

    }

    /**
     *  Creates a new directory path in the database
     * @param dirPath new dir path
     * @throws SQLException sql exception
     */
    public void addNewDirectory(File dirPath) throws SQLException {

        boolean exists;

        // Gets connection
        conn = getConnection();

        // Finds if a directory path is already defined in the database
        ResultSet rs = conn.createStatement().executeQuery("SELECT DirPath FROM directory");

        // Checks for existance
        exists = rs.isBeforeFirst();
        rs.close();

        PreparedStatement statement;
        // Exists, therefore use UPDATE where ID = 1
        if (exists)
        {
            // Creates statements and defines parameters
            statement = conn.prepareStatement("UPDATE directory SET DirPath = ? WHERE ID = 1");
            statement.setString(1, dirPath.getAbsolutePath());

            // Executes the statement
            statement.executeUpdate();

            // Closes the statement
            statement.close();

        }
        else { // Does not exist, therefore use INSERT INTO with ID = 1
            // Creates statements and defines parameters
            statement = conn.prepareStatement("INSERT INTO directory(ID, DirPath) VALUES(?, ?)");
            statement.setInt(1, 1);
            statement.setString(2, dirPath.getAbsolutePath());

            // Executes the statement
            statement.executeUpdate();

            // Closes the statement
            statement.close();
        }

        // Sets current directory path as the new one
        setNewDir(dirPath);

        conn.close();
    }

    /**
     * Sets the new picture directory
     * @param dir new directory
     */
    public void setNewDir(File dir) {
        this.dir = dir;
    }

    public String getCurrentDir() {
        String dirString;
        if (!System.getProperty("os.name").equals("Mac OS X")) // Checks which OS user is on
        {
            // Makes path according to user system
            dirString = dir + "\\";
        }
        else {
            // Makes path according to user system
            dirString = dir + "/";
        }

        return dirString;
    }


    /*|---------------------------------|
              FILE CHECK METHODS
      |---------------------------------|*/

    /**
     * Checks if format of the file is valid based on the allowedFormats ArrayList
     *
     * @param file file to be format checked
     * @return true or false based on success
     */
    public boolean formatCheck(File file) {
        //
        String format = file.getName().split("\\.")[file.getName().split("\\.").length - 1].toLowerCase();
        int amount = (int) allowedFormats.stream().filter(e -> e.contains(format)).count();
        // format supported if true
        return amount > 0;
    }

    /**
     * Checks if the file size is not above the given limit (current limit: 50 mb)
     * @param file file to be evaluated
     * @return true or false based on success
     */
    public boolean sizeCheck(File file) {
        double fileSize = (double) file.length()/(megabyte);
        // Checks if the file is within 50 megabytes
        return !(fileSize > uploadSizeLimit);
    }


    /*|---------------------------------|
             FILE RELATED METHODS
      |---------------------------------|*/


    /**
     * Returns an integer value based on the file status
     *
     * status = 3: there are both files in the folder which are not save and files in the database which are not loaded
     * status = 2: there are files in the folder which have not been saved to the database
     * status = 1: there are files in the database which have not been loaded into the folder yet
     * status = 0: everything is saved and upto date
     *
     * @return an integer value
     * @throws SQLException SQL exception
     */
    public int fileStatus() throws SQLException {
        conn = getConnection();

        ResultSet rs = conn.createStatement().executeQuery("SELECT FileName FROM picture");
        rs.next();
        int amountOfFilesInDB = 0;
        int amountOfLoadedFiles = 0;
        ArrayList<String> namesOfLocalStorageFiles = new ArrayList<>();

        for (Picture p : files)
        {
            namesOfLocalStorageFiles.add(p.getName());
        }

        rs.relative(-1);
        while (rs.next())
        {
            if (namesOfLocalStorageFiles.contains(rs.getString(1)))
            {
                amountOfLoadedFiles++;
            }
            amountOfFilesInDB++;
        }

        rs.close();

        conn.close();

        if (files.size() > amountOfLoadedFiles && amountOfFilesInDB > amountOfLoadedFiles)
        {
            // Unsaved files in folder and unloaded files in the database
            return 3;
        }
        else if (files.size() > amountOfLoadedFiles) {
            // Unsaved files in folder
            return 2;
        }
        else if (amountOfFilesInDB > amountOfLoadedFiles) {
            // Unloaded files in the database
            return 1;
        } else {
            // all files saved and upto date
            return 0;
        }
    }

    /**
     * Updates pictures in ArrayList by clearing the ArrayList and querying all the data found in the database
     * @throws SQLException SQL exception
     * @throws IOException IO exception
     */
    public void DownloadDataFromDB() throws SQLException, IOException {

        conn = getConnection();
        // Clears all previous data in the ArrayList

        ResultSet rs = conn.createStatement().executeQuery("SELECT * FROM picture");

        while (rs.next()) {

            if (getFileByName(rs.getString(2)) == null)
            {
                // Creates a new file based on filename
                String uploadTo;
                if (!System.getProperty("os.name").equals("Mac OS X")) // Checks which OS user is on
                {
                    // Makes path according to user system
                    uploadTo =  getCurrentDir() + rs.getString(2);
                }
                else {
                    // Makes path according to user system
                    uploadTo = getCurrentDir() + rs.getString(2);
                }
                File file = new File(uploadTo);
                // Prepares for file data write
                FileOutputStream fos = new FileOutputStream(file);
                byte[] b;
                Blob blob;
                blob = rs.getBlob(3);
                b = blob.getBytes(1, (int) blob.length());
                // Writes blob data into the file
                fos.write(b);

                fos.close();
            }

        }
        // Closes connection
        rs.close();
        conn.close();

        // So the pictures show up on search
        updateDataFromFolder();
        // Updates tags
        updateFileTags();
    }


    /**
     * Uploads the contents of the picture directory onto the DB
     * @throws FileNotFoundException file not found exception
     * @throws SQLException sql exception
     */
    public void uploadDataToDB() throws FileNotFoundException, SQLException, FileAlreadyExistsException {
        for (File file : dir.listFiles())
        {
            if (formatCheck(file))
            {
                if (getFileByName(file.getName()).getPictureID() == -1)
                {
                    uploadFileToDB(file);
                    updateIDOf(file);
                }

            }
        }
        updateFileTags();

    }

    /**
     * Attempts to upload the file to the database
     *
     * @param file the file to be uploaded
     * @return true or false based on upload success
     */
    public void uploadFileToDB(File file) throws FileNotFoundException, SQLException, FileAlreadyExistsException {

            if (!checkForDuplicates(file)) // Checks if any duplicates exist
            {
                // Defines variables
                PreparedStatement statement;
                FileInputStream inputStream = new FileInputStream(file);

                // Creates statements and defines parameters
                statement = conn.prepareStatement("INSERT INTO picture(FileName, Object) VALUES(?, ?)");
                statement.setString(1, file.getName());
                statement.setBinaryStream(2, inputStream, (int) (file.length()));

                // Executes and closes the statement
                statement.executeUpdate();
                statement.close();

            }
            else {
                throw new FileAlreadyExistsException(file.getName());
            }


            conn.close();
    }

    /**
     * Deletes file from database, folder and local storage, or folder and local storage depending on it's storage method (local or database)
     * @param image image to be deleted
     */
    public void deleteFile(Picture image) {
        if (image.getPictureID() != -1)
        {
            try {
                PreparedStatement statement;

                // Gets connection
                conn = getConnection();
                // Creates statements and defines parameters
                statement = conn.prepareStatement("DELETE FROM picture WHERE ID = ?");
                statement.setInt(1, image.getPictureID());

                // Executes and closes the statement
                statement.executeUpdate();
                statement.close();

                updateIDOf(new File(getCurrentDir() + image.getName()));

                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            // Removes image from local storage
            files.remove(image);
            deleteFileInDir(image);

        }

    }

    /**
     * Attempts to copy file over to the Pictures folder in resources
     *
     * @param file File to be copied over
     */
    public void copyFile(File file) throws IOException {

        String name = file.getName();
        String path = file.getAbsolutePath();

        // Attempts to copy the file to the folder
        Path original = Paths.get(path);
        Path destination = Paths.get( getCurrentDir() + name);
        Files.copy(original, destination, StandardCopyOption.REPLACE_EXISTING);
    }

    /**
     * Gathers the pictures from the picture directory and uploads them to the local storage in the application
     * @throws SQLException SQL exception
     */
    public void updateDataFromFolder() throws SQLException {

        ArrayList<String> picturesInLocalStorage = new ArrayList<>();
        for (Picture p : files)
        {
            picturesInLocalStorage.add(p.getName());
        }

        for (File file : dir.listFiles())
        {
            if (formatCheck(file))
            {
                if (!picturesInLocalStorage.contains(file.getName()))
                {
                    addToFiles(pictureFactory.createPicture(getIDOf(file), file));
                }

            }
        }

        updateFileTags();
    }

    /**
     * Uploads a file to the folder, making an object in the local storage and copying the file over to the folder
     * @param file the file to be uploaded
     * @throws IOException IO exception
     * @throws SQLException SQL exception
     */
    public void uploadFileToFolder(File file) throws IOException, SQLException {

        addToFiles(pictureFactory.createPicture(getIDOf(file), file));
        copyFile(file);

        updateFileTags();
    }

    /**
     * Checks if there are any duplicates / if a picture already exists in the database
     * @param file the file to be checked
     * @return true or false based on result (true = exists, false = does not exist)
     * @throws SQLException SQL exception
     */
    public boolean checkForDuplicates(File file) throws SQLException {
        PreparedStatement dupCheckStatement;

        // Gets connection
        conn = getConnection();

        // Creates duplication check statements and defines parameters
        dupCheckStatement = conn.prepareStatement("SELECT ID FROM picture WHERE FileName = ?");
        dupCheckStatement.setString(1, file.getName());

        // Executes query
        ResultSet dupCheckRs = dupCheckStatement.executeQuery();
        if (dupCheckRs.isBeforeFirst())
        {
            // Exists in db
            dupCheckRs.close();
            return true;
        }
        else {
            // Does not exist in db
            return false;
        }
    }

    /**
     * Gets an ID of a picture if it exists in the database
     * @param file the file to be checked for ID
     * @return the ID value
     * @throws SQLException SQL exception
     */
    public int getIDOf(File file) throws SQLException {

        if (checkForDuplicates(file))
        {
            PreparedStatement statement;

            // Gets connection
            conn = getConnection();

            // Creates duplication check statements and defines parameters
            statement = conn.prepareStatement("SELECT ID FROM picture WHERE FileName = ?");
            statement.setString(1, file.getName());

            // Executes query
            ResultSet rs = statement.executeQuery();
            rs.next();
            int id = rs.getInt(1);

            rs.close();

            conn.close();

            return id;
        } else {
            return -1;
        }
    }

    /**
     * Updates the ID of the provided file, by finding the ID and assigning it to that file
     * @param file the file of which to update the ID
     * @throws SQLException SQL exception
     */
    public void updateIDOf(File file) throws SQLException {
        for (Picture p : files)
        {
            if (file.getName().equals(p.getName()))
            {
                p.setPictureID(getIDOf(file));
            }
        }
    }


    /**
     * Adds file to the files ArrayList
     *
     * @param image the file to be added
     */
    public void addToFiles(Picture image) {
        files.add(image);
    }


    /**
     * Gets an object of class Model.Picture at a specified index
     *
     * @param index index
     * @return picture object
     */
    public Picture getFile(int index) {
        return files.get(index);
    }

    /**
     * Gets an object of Picture class by name
     * @param name name of the Picture
     * @return a Picture object or null
     */
    public Picture getFileByName(String name)
    {
        for (Picture p : files)
        {
            if (p.getName().equals(name))
            {
                return p;
            }
        }
        return null;
    }


    /*|---------------------------------|
                 TAGS OF FILES
      |---------------------------------|*/

    /**
     * Updates the tags in tagLists of all the pictures
     */
    public void updateFileTags() {

        // Clears all tags
        for (Picture p : files) {
            p.getTagList().clear();
        }

        try {
            // Gets connection
            conn = getConnection();

            // Finds the available Tags and their PictureID
            ResultSet rs = conn.createStatement().executeQuery("SELECT PictureID, Tag FROM tags");
            while (rs.next()) {
                for (int i = 0; i < files.size(); i++) {
                    Picture p = files.get(i);
                    if (p.getPictureID() != -1)
                    {
                        if (p.getPictureID() == rs.getInt(1)) {
                            p.addToTagList(rs.getString(2));
                            i = files.size();
                        }
                    }

                }
            }

            rs.close();

            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
     * Adds the tag to the picture
     *
     * @param image picture of the new tag
     * @param tag   tag to be added
     */
    public void addTag(Picture image, String tag) {

        if (!tag.isEmpty())
        {
            try {
                PreparedStatement statement;

                // Gets connection
                conn = getConnection();

                // Creates statements and defines parameters
                statement = conn.prepareStatement("SELECT * FROM tags WHERE PictureID = ? AND Tag = ?");
                statement.setInt(1, image.getPictureID());
                statement.setString(2, tag.toLowerCase());

                // Executes and closes the statement
                if (!statement.executeQuery().next()) // Checks for tag duplicates
                {
                    PreparedStatement addStatement;
                    // Creates statements and defines parameters
                    addStatement = conn.prepareStatement("INSERT INTO tags(PictureID, Tag) VALUES(?, ?)");
                    addStatement.setInt(1, image.getPictureID());
                    addStatement.setString(2, tag.toLowerCase());

                    // Executes the statement
                    addStatement.executeUpdate();
                    // Closes the statement
                    addStatement.close();

                    // Adds tag from tagList
                    image.addTag(tag.toLowerCase());
                }

                // Closes the statement
                statement.close();

                conn.close();
            } catch (SQLException ex) {
                System.out.println("Sql exception");
                ex.printStackTrace();
            }
        }

    }


    /**
     * Deletes the tag from a picture
     *
     * @param image the picture containing the tag
     * @param tag   tag to be deleted
     */
    public void deleteTag(Picture image, String tag) {

        try {
            PreparedStatement statement;

            // Gets connection
            conn = getConnection();
            // Creates statements and defines parameters
            statement = conn.prepareStatement("DELETE FROM tags WHERE PictureID = ? AND Tag = ?");
            statement.setInt(1, image.getPictureID());
            statement.setString(2, tag);

            // Executes and closes the statement
            statement.executeUpdate();
            statement.close();

            // Removes tag from tagList
            image.removeFromTagList(tag);

            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /*|---------------------------------|
           SEARCHING RELATED METHODS
      |---------------------------------|*/
    /**
     * Uses a searchString to search through the files ArrayList and finds any file that contains the searchString in it's file name
     *
     * @param searchString The search string
     * @return list of Model.Picture objects
     */
    public List<Picture> search(String searchString, boolean landscape, boolean portrait, ArrayList<String> selectedSearchTags, int fileSizeMin, int fileSizeMax, int resolutionMin, int resolutionMax, int focalLengthMin, int focalLengthMax, int brightnessMin, int brightnessMax, int isoMin, int isoMax) {


        Iterator<Picture> iterator;
        ArrayList<Picture> searchFiles = (ArrayList<Picture>) files.clone();

        // Checks if any Model.CameraPicture specific modifiers are used
        if (focalLengthMin > 0 || brightnessMin > 0 || isoMin > 0) {

            // Removes all objects of subclass Model.ComputerPicture as they do not contain these search fields
            searchFiles.removeIf(p -> p instanceof ComputerPicture);

        }

        // Evaluates
        if (!landscape || !portrait)  // NAND gate, if both are true then it does not have to iterate as it takes everything
        {
            iterator = searchFiles.iterator();

            while (iterator.hasNext()) {
                Picture p = iterator.next();
                int width = Integer.parseInt(p.getResolution().split("x")[0]);
                int height = Integer.parseInt(p.getResolution().split("x")[1]);
                if (landscape) // User wants to display landscape pictures
                {
                    if (height > width) // Looks for pictures which are portraits and removes them
                    {
                        iterator.remove();
                    }
                }
                else if (portrait) // User wants to display portrait pictures
                {
                    if (width > height) // Looks for pictures which are landscapes and removes them
                    {
                        iterator.remove();
                    }
                }
                else { // User has deselected both options, meaning only cubic pictures match the search
                    if (width != height) // Looks for pictures which are not cubic and removes them
                    {
                        iterator.remove();
                    }
                }
            }

        }


        // Evaluates by tags provided
        if (selectedSearchTags.size() > 0)
        {
            iterator = searchFiles.iterator();
            while (iterator.hasNext()) {
                Picture p = iterator.next();
                int tagCheck = 0;
                for (String tag : selectedSearchTags)
                {
                    if (p.getTagList().contains(tag))
                    {
                        tagCheck++;
                    }
                }
                if (tagCheck < 1)
                {
                    iterator.remove();
                }
            }
        }


        // Evaluates by File size
        iterator = searchFiles.iterator();
        while (iterator.hasNext()) {

            Picture p = iterator.next();
            double fileSize;

            // Checks the biggest value to determine if to evaluate file size in Megabytes or Kilobytes
            if (fileSizeSliderMultiplier())
            {
                fileSize = p.getFileSize() / megabyte;
            }
            else {
                fileSize = p.getFileSize() / kilobyte;
            }

            if (fileSize < fileSizeMin || fileSize > fileSizeMax) {
                iterator.remove();
            }

        }


        // Evaluates by resolution
        iterator = searchFiles.iterator();
        while (iterator.hasNext()) {
            Picture p = iterator.next();
            int height = Integer.parseInt(p.getResolution().split("x")[1]);
            if (resolutionMax == 2160)  // If max resolution is 2160p, meaning it displays all pictures, 2160p and above
            {
                if (height < resolutionMin) {
                    iterator.remove();
                }
            }
            else {
                if (height < resolutionMin || height > resolutionMax) {
                    iterator.remove();
                }
            }


        }


        // Evaluates by name
        iterator = searchFiles.iterator();
        while (iterator.hasNext()) {
            Picture p = iterator.next();
            if (!p.getName().toLowerCase().contains(searchString.toLowerCase())) {
                iterator.remove();
            }

        }


        // Evaluates by Focal length
        iterator = searchFiles.iterator();
        while (iterator.hasNext()) {
            Picture p = iterator.next();
            if (p instanceof CameraPicture) {
                CameraPicture castP = (CameraPicture) p;
                if (castP.getFocalLength() < focalLengthMin || castP.getFocalLength() > focalLengthMax) {
                    iterator.remove();
                }
            }

        }


        // Evaluates by Brightness value
        iterator = searchFiles.iterator();
        while (iterator.hasNext()) {
            Picture p = iterator.next();
            if (p instanceof CameraPicture) {
                CameraPicture castP = (CameraPicture) p;
                if (castP.getBrightnessValue() < brightnessMin || castP.getBrightnessValue() > brightnessMax) {
                    iterator.remove();
                }
            }

        }


        // Evaluates by ISO
        iterator = searchFiles.iterator();
        while (iterator.hasNext()) {
            Picture p = iterator.next();
            if (p instanceof CameraPicture) {
                CameraPicture castP = (CameraPicture) p;
                if (castP.getIso() < isoMin || castP.getIso() > isoMax) {
                    iterator.remove();
                }
            }

        }

        return searchFiles;
    }

    /**
     * Decides if the file size slider should use kilobytes or megabytes based on biggest file size
     *
     * @return true or false based on which one it will use
     */

    public boolean fileSizeSliderMultiplier() {
        long max = files.stream().mapToLong(Picture::getFileSize).max().orElseThrow(NoSuchElementException::new);
        // Checks if biggest file size is over x Megabytes
        // Megabytes if true, Kilobytes if false
        return (max / megabyte) > fileSizeSliderLimit;
    }

    /**
     * Gets the smallest file size in files, else return 0
     *
     * @return file size in megabytes or kilobytes
     */
    public int getMinFileSize() {
        if (files.size() > 0) {

            // Finds the lowest file size
            long min = files.stream().mapToLong(Picture::getFileSize).min().orElseThrow(NoSuchElementException::new);

            if (fileSizeSliderMultiplier()) // Checks if biggest file size is over x Megabytes
            {

                return (int) Math.floor((double) min / megabyte);
            }
            else {

                return (int) Math.floor((double) min / kilobyte);
            }

        } else {
            // No pictures currently in the database
            return 0;
        }
    }

    /**
     * Gets the biggest file size in files, else return 0
     *
     * @return file size in megabytes or kilobytes
     */
    public int getMaxFileSize() {
        if (files.size() > 0) {

            // Finds the lowest file size
            long max = files.stream().mapToLong(Picture::getFileSize).max().orElseThrow(NoSuchElementException::new);

            // Checks if to return file size in Megabytes or Kilobytes
            if (fileSizeSliderMultiplier()) // Checks if biggest file size is over x Megabytes
            {
                return (int) Math.ceil((double) max / megabyte);
            }
            else {
                return (int) Math.ceil((double) max / kilobyte);
            }

        } else {
            // No pictures currently in the database
            return 0;
        }
    }

    /**
     * Gets the smallest ISO in files, else return 0
     *
     * @return smallest ISO value
     */
    public int getMinISO() {

        if (files.size() > 0) {
            ArrayList<CameraPicture> objectsContainingISO = new ArrayList<>();

            for (Picture p : files) {
                // Checks for pictures of sbuclass Model.CameraPicture
                if (p instanceof CameraPicture) {
                    objectsContainingISO.add((CameraPicture) p);
                } else {
                    // A Model.ComputerPicture object is found, meaning lower ISO is 0
                    return 0;
                }

            }

            // The for loop did not return 0, meaning there is at least one Model.CameraPicture object, therefore no need to check for Arraylist size

            // Finds the lowest ISO value
            return objectsContainingISO.stream().mapToInt(CameraPicture::getIso).min().orElseThrow(NoSuchElementException::new);
        } else {
            // No pictures currently in the database
            return 0;
        }

    }

    /**
     * Gets the biggest ISO in files, else return 0
     *
     * @return smallest ISO value
     */
    public int getMaxISO() {

        if (files.size() > 0) {
            ArrayList<CameraPicture> objectsContainingISO = new ArrayList<>();

            for (Picture p : files) {
                // Checks for pictures of sbuclass Model.CameraPicture
                if (p instanceof CameraPicture) {
                    objectsContainingISO.add((CameraPicture) p);
                }

            }

            // Checks if ArrayList containing object with ISO values is not empty
            if (objectsContainingISO.size() > 0) {
                // Finds the highest ISO value
                int max = objectsContainingISO.stream().mapToInt(CameraPicture::getIso).max().orElseThrow(NoSuchElementException::new);
                int upperMax = (max-(max%10)+10);
                if (upperMax == (max+10))
                {
                    return max;
                }
                else {
                    return upperMax;
                }
            } else {
                // No Model.CameraPicture objects found, meaning highest ISO is 0
                return 0;
            }
        } else {
            // No pictures currently in the database
            return 0;
        }


    }

    /**
     * Gets the smallest focal length in files, else return 0
     *
     * @return smallest focal length
     */
    public int getMinFocalLength() {
        if (files.size() > 0) {
            ArrayList<CameraPicture> objectsContainingFocalLength = new ArrayList<>();

            for (Picture p : files) {
                // Checks for pictures of sbuclass Model.CameraPicture
                if (p instanceof CameraPicture) {
                    objectsContainingFocalLength.add((CameraPicture) p);
                } else {
                    // A Model.ComputerPicture object is found, meaning lower Focal length is 0
                    return 0;
                }

            }

            // The for loop did not return 0, meaning there is at least one Model.CameraPicture object, therefore no need to check for Arraylist size

            // Finds the lowest Focal length value
            return objectsContainingFocalLength.stream().mapToInt(e -> (int) Math.floor(e.getFocalLength())).min().orElseThrow(NoSuchElementException::new);
        } else {
            // No pictures currently in the database
            return 0;
        }
    }

    /**
     * Gets the biggest focal length, else return 0
     *
     * @return biggest focal length
     */
    public int getMaxFocalLength() {
        if (files.size() > 0) {
            ArrayList<CameraPicture> objectsContainingFocalLength = new ArrayList<>();

            for (Picture p : files) {
                // Checks for pictures of sbuclass Model.CameraPicture
                if (p instanceof CameraPicture) {
                    objectsContainingFocalLength.add((CameraPicture) p);
                }

            }

            // Checks if ArrayList containing object with Focal length values is not empty
            if (objectsContainingFocalLength.size() > 0) {
                // Finds the highest ISO value
                int max = objectsContainingFocalLength.stream().mapToInt(e -> (int) Math.ceil(e.getFocalLength())).max().orElseThrow(NoSuchElementException::new);
                int upperMax = (max-(max%10)+10);
                if (upperMax == (max+10))
                {
                    return max;
                }
                else {
                    return upperMax;
                }
            } else {
                // No Model.CameraPicture objects found, meaning highest Focal length is 0
                return 0;
            }
        } else {
            // No pictures currently in the database
            return 0;
        }
    }

    /**
     * Gets the smallest brightness value, else return 0
     *
     * @return smallest brightness value
     */
    public int getMinBrightnessValue() {
        if (files.size() > 0) {
            ArrayList<CameraPicture> objectsContainingBrightnessValue = new ArrayList<>();

            for (Picture p : files) {
                // Checks for pictures of sbuclass Model.CameraPicture
                if (p instanceof CameraPicture) {
                    objectsContainingBrightnessValue.add((CameraPicture) p);
                } else {
                    // A Model.ComputerPicture object is found, meaning lower Brightness value is 0
                    return 0;
                }

            }

            // The for loop did not return 0, meaning there is at least one Model.CameraPicture object, therefore no need to check for Arraylist size

            // Finds the lowest Brightness value
            return objectsContainingBrightnessValue.stream().mapToInt(e -> (int) Math.floor(e.getBrightnessValue())).min().orElseThrow(NoSuchElementException::new);
        } else {
            // No pictures currently in the database
            return 0;
        }
    }

    /**
     * Gets the biggest brightness value, else return 0
     *
     * @return biggest brightness value
     */
    public int getMaxBrightnessValue() {

        if (files.size() > 0) {
            ArrayList<CameraPicture> objectsContainingBrightnessValue = new ArrayList<>();

            for (Picture p : files) {
                // Checks for pictures of sbuclass Model.CameraPicture
                if (p instanceof CameraPicture) {
                    objectsContainingBrightnessValue.add((CameraPicture) p);
                }

            }

            // Checks if ArrayList containing object with Brightnesss values is not empty
            if (objectsContainingBrightnessValue.size() > 0) {
                // Finds the highest Brightness value
                int max = objectsContainingBrightnessValue.stream().mapToInt(e -> (int) Math.ceil(e.getBrightnessValue())).max().orElseThrow(NoSuchElementException::new);
                int upperMax = (max-(max%10)+10);
                if (upperMax == (max+10))
                {
                    return max;
                }
                else {
                    return upperMax;
                }
            } else {
                // No Model.CameraPicture objects found, meaning highest Brightness value is 0
                return 0;
            }
        } else {
            // No pictures currently in the database
            return 0;
        }
    }

    /**
     * Gets all the tags available in the database grouped by name
     *
     * @return an arrayList containing all the tag names
     */
    public ArrayList<String> getSearchTags() {

        ArrayList<String> searchTags = new ArrayList<>();

        try {
            // Gets connection
            conn = getConnection();

            // Finds the available Tags and their PictureID
            ResultSet rs = conn.createStatement().executeQuery("SELECT Tag FROM tags GROUP BY Tag");
            while (rs.next()) {
                searchTags.add(rs.getString(1));
            }

            rs.close();

            conn.close();
        } catch (SQLException ex) {
            System.out.println("SQL exception");
            ex.printStackTrace();
        }
        return searchTags;

    }

    /*|---------------------------------|
               DIRECTORY METHODS
      |---------------------------------|*/

    /**
     * Deletes all files within the (picture) folder
     *
     */
    public void cleanDir() {

        for (File file : Objects.requireNonNull(dir.listFiles()))
            if (!file.isDirectory())
            {
                file.delete();
            }
    }


    /**
     * Deletes a specific file in the picture directory
     *
     * @param fileToDelete file to be deleted
     */
    public void deleteFileInDir(Picture fileToDelete) {
        for (File file : Objects.requireNonNull(dir.listFiles()))
            if (file.getName().equals(fileToDelete.getName()))
            {
                file.delete();
            }

    }


    /*|---------------------------------|
                 MISC METHODS
      |---------------------------------|*/

    /**
     * Exports the images selected to be exported to a pdf
     *
     * @param exportLocation the location to export the pdf to
     */
    public void exportToPDF(File exportLocation) throws IOException, DocumentException {

        ArrayList<Picture> pdfExportFiles = new ArrayList<>();
        // Makes a list of all files to export to pdf
        for (Picture p : files)
        {
            if (p.getPDFExport())
            {
                pdfExportFiles.add(p);
            }
        }

        if (pdfExportFiles.size() > 0)
        {
            Document document = new Document();
            // PdfWriter instance
            PdfWriter.getInstance(document, new FileOutputStream(exportLocation));

            document.open();

            for (Picture p : pdfExportFiles) {
                document.newPage();


                // Gets picture as an instance of Image
                Image image = Image.getInstance(new File(getCurrentDir() + p.getName()).getAbsolutePath());
                // Scales image
                float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                        - document.rightMargin()) / image.getWidth()) * 100;

                image.scalePercent(scaler);
                document.add(image);
            }

            document.close();
        }
        else {
            throw new NullPointerException();
        }

    }


    /**
     * Gets the amount of elements in the files array
     *
     * @return amount of elements
     */
    public int getSize() {
        return files.size();
    }

    /**
     * Returns all the allowed formats as a string
     *
     * @return String of allowed formats
     */
    public String allowedFormatsToString()
    {
        return allowedFormats.toString();
    }

    /**
     * Gets the upload size limit variable
     * @return upload size limit
     */
    public double getUploadSizeLimit()
    {
        return uploadSizeLimit;
    }
}
