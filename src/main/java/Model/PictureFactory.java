package Model;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Image;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;


/**
 * Responsible for creating subclass objects of superclass Model.Picture based on their available details
 *
 * @author IDATG1002 Group 5
 */
public class PictureFactory {

    private static PictureFactory instance = new PictureFactory();

    private Metadata fileData = null;

    private PictureFactory() {

    }

    /**
     * Gets instance of this class
     * @return instance of the class
     */
    public static PictureFactory getInstance() {
        return instance;
    }

    /**
     * Creates a picture object of subclass Model.CameraPicture or Model.ComputerPicture based on the metadata available
     *
     * @param ID   ID of the object
     * @param file File associated with the ID
     * @return The created picture object
     */
    public Picture createPicture(int ID, File file) {


        // Attempts to create a new Model.Metadata object
        fileData = Metadata.getInstance(file);

        // Object variables
        // Using BufferedImage class is an easier than having to make a new directory for each image format
        String resolution = "";
        try {
            Image image = Image.getInstance(String.valueOf(file));
            int height, width;
            height = (int) image.getHeight();
            width = (int) image.getWidth();
            resolution = width + "x" + height;
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (BadElementException ex) {
            System.out.println("Bad element exception");
            ex.printStackTrace();
        }


        // Name and path
        String name, path;
        name = file.getName();
        path = file.toURI().toString();

        // File size
        long fileSize = fileData.getFileSize();

        // Checks if the image has a EXIF directory / has photographic data
        if (fileData.getEXIFDir() != null && fileData.getEXIFDir().getTagCount() > 4) // EXIF directory found and has sufficient amount of tags to derive data from
        {
            // Constructs and returns Model.CameraPicture object
            return ConstructCameraPicture(ID, name, path, resolution, fileSize);
        } else { // EXIF directory not found
            // Returns Model.ComputerPicture object
            return new ComputerPicture(ID, name, path, resolution, fileSize);
        }

    }

    /**
     * Constructs a Model.CameraPicture object
     *
     * @param ID           ID of the object
     * @param name         Name of the picture
     * @param path         Path of the picture
     * @param resolution   Resolution of the picture
     * @param fileSize     Model.Picture file size
     * @return A Model.CameraPicture object
     */
    public CameraPicture ConstructCameraPicture(int ID, String name, String path, String resolution, long fileSize) {


        // Camera picture specific variables
        // Time taken
        Date dateTimeTaken;
        dateTimeTaken = fileData.getDateTaken();

        LocalDateTime timeTaken;
        if (dateTimeTaken != null) {
            // Parsing to LocalDateTime
            timeTaken = LocalDateTime.ofInstant(dateTimeTaken.toInstant(), ZoneId.systemDefault());
        } else {
            timeTaken = null;
        }

        // Focal length
        double focalLength;
        focalLength = fileData.getFocalLength();

        // ISO speed rating
        int iso;
        iso = fileData.getISO();

        // Aperture value
        String aperture;
        aperture = fileData.getApertureValue();

        // Exposure time
        String exposureTime;
        exposureTime = fileData.getExposureTime();

        // Make
        String make;
        make = fileData.getMake();

        // Model
        String model;
        model = fileData.getModel();

        // Exposure mode
        String exposureMode;
        exposureMode = fileData.getExposureMode();

        // Shutter speed value
        String shutterSpeedValue;
        shutterSpeedValue = fileData.getShutterSpeedValue();

        // Software
        String software;
        software = fileData.getSoftware();

        // Artist
        String artist;
        artist = fileData.getArtist();

        // Flash
        String flash;
        flash = fileData.getFlash();

        // Brightness value
        double brightnessValue;
        brightnessValue = (double) Math.round(fileData.getBrightnessValue()*100)/100;

        // Color space
        String colorSpace;
        colorSpace = fileData.getColorSpace();

        // Returns Model.CameraPicture object
        return new CameraPicture(ID, name, path, resolution, fileSize, timeTaken, focalLength, aperture, exposureTime, iso, make, model, exposureMode, shutterSpeedValue, software, artist, flash, brightnessValue, colorSpace);
    }
}
