package Model;

/**
 * Responsible for storage of data of a computer picture
 *
 * @author IDATG1002 Group 5
 */
public class ComputerPicture extends Picture {
    public ComputerPicture(int pictureID, String name, String path, String resolution, long fileSize) {
        super(pictureID, name, path, resolution, fileSize);
    }
}
