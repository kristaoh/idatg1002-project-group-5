# Description
The Image Processing Program (or IPP for short) is a program used for extracting metadata from images and storing them in a searchable database. 
>  Notice: This program is not optimised and will run slower based on how many files are present. 

# Gettings started:

*  Make sure to read the project report for login details
*  An internet connection
*  A computer running newest version of Java

If you want to get this program either clone this repository or go to our [link release page](https://gitlab.stud.idi.ntnu.no/kristaoh/idatg1002-project-group-5/-/releases) and download the .jar.

If you already have access to the source kode or the .jar file, then you can start using the program by simple running the .jar file or opening our source code in an IDE (preferably IntelliJ).

# User Guide:

### **Uploading:**
*Supported file formats: jpeg, png, gif, ico, bmp, jpg*
1) Click [Upload], and follow the necessary steps when selecting an image.
2) Click [Confirm] if this is the desired image, [Cancel] if not.
3) Image is now uploaded, and you can add tags if desired.

### **Adding tags**
1) Click [Edit] when previewing an image.
2) Write a suitable name for the tag.
2) When done, Click [Done].

### **Exporting:**
Click [Add Mode], which enables the selection mode.
Click [Select] on the individual images to add them.
Click [Export to PDF], and choose a name and location.

### **Operating Search:**
On the top of the page, you'll find the search bar.
On the left, there is a panel for adjusting the search parameters.
Below you'll see the tag panel, you can here select images based on existing tags.
Above you'll see the tool bar, you can here enable Edit or Add, and export images.
If you want to take a closer look at an image, just click on the square preview and you'll be directed to the preview page.

# Licencing:
In this project, we've used a couple of external libraries to get the desired functionallity. These are: 

#### Icons8
Every icon, in exception to our logo, has been found on Icons8:
https://icons8.com/icons/material-rounded

#### controlsfx
https://github.com/controlsfx/controlsfx

#### metadata-extractor
https://github.com/drewnoakes/metadata-extractor

####  xmpcore
https://mvnrepository.com/artifact/com.adobe.xmp/xmpcore/6.0.6

#### mysql-connector-java
https://mvnrepository.com/artifact/mysql/mysql-connector-java/5.1.40

#### itextdoc
https://mvnrepository.com/artifact/com.itextpdf.maven/itextdoc/2.0.0

# Possible problems encountered while using IDE:

|------ JDK/SDK ------|

Our IDE of choice (IntelliJ) sometimes messed up what langauge level and/or JDK/SDK the project uses.

To fix the langauge level issue, make sure that the language level is set to 8:
File -> Project Settings -> Project -> Project language level 
or
File -> Settings -> Build, Execution, Deployment -> Compiler -> Java Compiler and change the target bytecode to *Same as language level*

IntelliJ also messed up which module to use, a fix for this is:
Run -> Edit Configurations -> Use classpath for module and set this to the avaiable module


|------ Libary -------|

Make sure the necessary libraries can be found in the external libraries section in the project tree.